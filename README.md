# Publicis Website

Publicis Website has built using Wordpress. There is a build system can be setup for development process under " wp-content/themes/rebirth-jellythemes ".

## Stack

```
Node.js
PHP
MySQL
```

## Required global npm packages

```
grunt-cli run: $sudo npm install -g grunt-cli
compass run: $sudo gem install compass
```

## Setup Localy
* clone the repository to your machine.
* make sure you have Node.js and MAMP running on your system and grunt-cli and compass installed globally.
* Point the project folder to the MAMP application.
* make sure MySQL is using the most recent database (coming soon...)
* navigate to theme folder " wp-content/themes/rebirth-jellythemes " and run: **npm install**
* after the packages installed and MAMP already running on your local server run: **grunt**
* now grunt is watching changes under folders (this needs livereload browser plugin to automatically reload local)
⋅⋅* " src/js "
⋅⋅* " src/scss "
* to keep things organize if you are going to manipulate specific module (if not exist) create a new .sass file under the folder " src/scss " and import the file inside " styles.scss "