module.exports = function(grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        concat: { 
			dist: {
				src: ['src/js/*.js'],
				dest: 'Publicis/scripts/production.js'
			 }
        },
        uglify: {
            build: {
                src: 'Publicis/scripts/production.js',
                dest: 'Publicis/scripts/production.min.js'
            }
        },
		jshint: {
			all: ['src/js/*.js']
        },
        sass: {
            dist: {
                options: {
                    style: 'expanded'
                },
                files: {
                    'Publicis/style.css': 'src/styles.scss'
                }
            } 
        },
        cssmin: {
            target: {
                files: [{
                    expand: true,
                    cwd: 'Publicis',
                    src: ['*.css', '!*.min.css'],
                    dest: 'Publicis',
                    ext: '.min.css'
                }]
            }
        },
        watch: {
            options: {
				hostname: "localhost", 
				port: 8888,
                livereload: true,
            },
            scripts: {
                files: ['src/js/*.js'],
                tasks: ['jshint', 'concat', 'uglify'],
                options: {
                    spawn: false,
                },
            },
            css: {
                files: ['src/scss/main/*.scss', 'src/scss/production/*.scss'],
                tasks: ['sass', 'cssmin'],
                options: {
                    spawn: false,
                }
            },
        }
    });
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-cssmin');

    grunt.registerTask('default', ['watch']);
};