<?php get_header(); ?>
<?php $rebirth_jellythemes = rebirth_jellythemes_theme_options();?>
<!-- INTRO -->

<div class="intro jIntro">
    <div class="image-cover menu-bottom" style="background-image:url(/wp-content/uploads/2017/05/news_header_02.jpg);">
        <div class="vcenter text-center">
            <div class="container">
                <div class="row visible">
                    <div class="col-md-8 col-md-offset-2">
                        <div class="voffset50"></div>
                        <p class="post-primary-title invert">We can’t seem to find the page you were <span style="display:inline-block;">looking for.</span></p>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>
