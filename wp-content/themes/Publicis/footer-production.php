<!-- FOOTER -->
<?php $jellythemes = rebirth_jellythemes_theme_options(); ?>
<footer>
  <div class="copyright">
    <div class="voffset40"></div>
    <p><?php echo date('Y'); ?> Publicis Canada.</p>
    <div class="voffset40"></div>
  </div>
</footer>
<?php if ($jellythemes['hamb_menu']): ?>
  <div class="overlay overlay-hugeinc">
  <?php wp_nav_menu(array(
          'container' => 'nav',
          'menu_class' => '',
          'container_id' => 'nav',
          'container_class' => '',
          'theme_location' => 'main',
          'walker' => new rebirth_jellythemes_walker_nav_menu)); ?>
  </div>
<?php endif ?>
<!-- SCRIPTS -->
<?php wp_footer(); ?>

</body>
</html>
