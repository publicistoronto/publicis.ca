<!-- FOOTER -->
<?php $jellythemes = rebirth_jellythemes_theme_options(); ?>
<footer>
  <div class="container-fluid">
    <div class="voffset30"></div>
      <div class="row">
          <div class="col-md-12 col-sm-12 logo-container">
            <div class="voffset30"></div>
            <a class="navbar-brand" title="Publicis Canada" href="<?php echo esc_url(home_url('/'));?>"><img class="publicis-logo-footer" src="<?php echo get_stylesheet_directory_uri() ?>/images/demo/logo-alt.png" alt="publicis Canada"/></a>
            
          </div>
      </div>
      <div class="row">
      <div class="col-md-4 col-sm-12">
        <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Footer Content Col1") ) : ?>
        <?php endif;?>
        <div class="column1">
          <h2>Follow Us</h2>
            <div class="socialmedia-buttons smw_left">
            <a href="https://www.facebook.com/PublicisWWintheUSA" rel="nofollow" target="_blank"><img width="32" height="32" src="<?php echo get_stylesheet_directory_uri() ?>/images/icons/footer-icons/facebook.png" alt="Follow Us on Facebook" title="Follow Us on Facebook"></a>
            <a href="https://twitter.com/PublicisNA" rel="nofollow" target="_blank"><img width="32" height="32" src="<?php echo get_stylesheet_directory_uri() ?>/images/icons/footer-icons/twitter.png" alt="Follow Us on Twitter" title="Follow Us on Twitter"></a>
            <a href="https://www.linkedin.com/company/pubcanada/" rel="nofollow" target="_blank"><img width="32" height="32" src="<?php echo get_stylesheet_directory_uri() ?>/images/icons/footer-icons/linkedin.png" alt="Follow Us on LinkedIn" title="Follow Us on LinkedIn"></a>
            <a href="https://www.instagram.com/publicisna/?hl=en" rel="nofollow" target="_blank"><img width="32" height="32" src="<?php echo get_stylesheet_directory_uri() ?>/images/icons/footer-icons/instagram.png" alt="Follow Us on Instagram" title="Follow Us on Instagram"></a>
            </div>
        </div>
      </div>
      <div class="col-md-4 col-sm-12">
        <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Footer Content Col2") ) : ?>
		    <?php endif;?> 
      </div>
      <div class="col-md-4 col-sm-12">
        <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Footer Content Col3") ) : ?>
		    <?php endif;?> 
      </div>
    </div>
    <div class="voffset30"></div>
  </div>
  <div class="copyright">
    <div class="voffset40"></div>
    <p><?php echo date('Y'); ?> Publicis Canada.</p>
    <div class="voffset40"></div>
  </div>
</footer>
<?php if ($jellythemes['hamb_menu']): ?>
  <div class="overlay overlay-hugeinc">
  <?php wp_nav_menu(array(
          'container' => 'nav',
          'menu_class' => '',
          'container_id' => 'nav',
          'container_class' => '',
          'theme_location' => 'main',
          'walker' => new rebirth_jellythemes_walker_nav_menu)); ?>
  </div>
<?php endif ?>
<!-- SCRIPTS -->
<?php wp_footer(); ?>
</body>
</html>
