<?php

    /* rewrite rebirth-works slog to our_work. dash seperator is not allowed for this slug. should use underscore. */

    function change_post_types_slug( $args, $post_type ) {
        if ( 'rebirth-works' === $post_type ) {
            $args['rewrite']['slug'] = 'our_work';
        }
        return $args;
    }
    add_filter( 'register_post_type_args', 'change_post_types_slug', 10, 2 );

    /* adding the footer text widget area */
	if ( function_exists('register_sidebar') )
	  register_sidebar(array(
        'id' => 'footer-content-col-1',
        'name' => 'Footer Content Col1',
		'before_widget' => '<div class = "column1">',
		'after_widget' => '</div>',
		'before_title' => '<h2>',
		'after_title' => '</h2>',
	  )
	);
	if ( function_exists('register_sidebar') )
	  register_sidebar(array(
        'id' => 'footer-content-col-2',
		'name' => 'Footer Content Col2',
		'before_widget' => '<div class = "column2">',
		'after_widget' => '</div>',
		'before_title' => '<h2>',
		'after_title' => '</h2>',
	  )
	);
	if ( function_exists('register_sidebar') )
	  register_sidebar(array(
        'id' => 'footer-content-col-3',
		'name' => 'Footer Content Col3',
		'before_widget' => '<div class = "column3">',
		'after_widget' => '</div>',
		'before_title' => '<h2>',
		'after_title' => '</h2>',
	  )
	);
    /*-------------------------------------------------------*/
    /*       Add style to custom fields admin side           */
    /*-------------------------------------------------------*/
    add_action('admin_head', 'works_tabs');
    function works_tabs() {
        echo '<style type="text/css">
            #acf_139 .field_type-text, #acf_139 .field_type-image { width:40%; border:none !important; display:inline-block; }
            #acf-text1 input, #acf-text2 input, #acf-text3 input { height:auto; min-height:200px; vertical-align:top; }
        </style>';
    }
    // add_action('wp_enqueue_style', 'rebirth_jellythemes_scripts_and_styles');


    /**
     * styles and scripts
    */

    function publicis_child_scripts() {
        // Get last modified timestamp of CSS file in /css/style.css
        $ver = date("Ymdhms");

        wp_dequeue_style( 'rebirth-jellythemes-style' );
        wp_deregister_style( 'rebirth-jellythemes-style' );
        wp_enqueue_script('TweenMax', 'https://cdnjs.cloudflare.com/ajax/libs/gsap/2.0.2/TweenMax.min.js', array(),false,true );
        wp_enqueue_script('ScrollMagic', 'https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.6/ScrollMagic.min.js', array(),false,true );
        wp_enqueue_script('scrollTo', 'https://cdnjs.cloudflare.com/ajax/libs/gsap/2.1.2/plugins/ScrollToPlugin.min.js', array(),false,true );
        wp_enqueue_script('jQueryEasing', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js', array(),false,true );
        wp_enqueue_script('animationGsap', 'https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.6/plugins/animation.gsap.min.js', array(),false,true );
        wp_enqueue_script('custom-js', get_stylesheet_directory_uri() . '/scripts/production.min.js?v=', array(), $ver, true );
        wp_enqueue_style( 'custom-child', get_stylesheet_directory_uri() . '/style.min.css?v', array(), $ver, "all" );
        wp_enqueue_style( 'fontawesome', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css',false,'1.1','all');

    }
    add_action('wp_enqueue_scripts', 'publicis_child_scripts', 20);
    
    
    

    // Remove emoji library

    remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
    remove_action( 'wp_print_styles', 'print_emoji_styles' );
    
    // minify html

    class WP_HTML_Compression{
        // Settings
        protected $compress_css = true;
        protected $compress_js = true;
        protected $info_comment = true;
        protected $remove_comments = true;

        // Variables
        protected $html;
        public function __construct($html)
        {
         if (!empty($html))
         {
             $this->parseHTML($html);
         }
        }
        public function __toString()
        {
         return $this->html;
        }
        protected function bottomComment($raw, $compressed)
        {
         $raw = strlen($raw);
         $compressed = strlen($compressed);

         $savings = ($raw-$compressed) / $raw * 100;

         $savings = round($savings, 2);

         return '<!--HTML compressed, size saved '.$savings.'%. From '.$raw.' bytes, now '.$compressed.' bytes-->';
        }
        protected function minifyHTML($html)
        {
         $pattern = '/<(?<script>script).*?<\/script\s*>|<(?<style>style).*?<\/style\s*>|<!(?<comment>--).*?-->|<(?<tag>[\/\w.:-]*)(?:".*?"|\'.*?\'|[^\'">]+)*>|(?<text>((<[^!\/\w.:-])?[^<]*)+)|/si';
         preg_match_all($pattern, $html, $matches, PREG_SET_ORDER);
         $overriding = false;
         $raw_tag = false;
         // Variable reused for output
         $html = '';
         foreach ($matches as $token)
         {
             $tag = (isset($token['tag'])) ? strtolower($token['tag']) : null;

             $content = $token[0];

             if (is_null($tag))
             {
                 if ( !empty($token['script']) )
                 {
                     $strip = $this->compress_js;
                 }
                 else if ( !empty($token['style']) )
                 {
                     $strip = $this->compress_css;
                 }
                 else if ($content == '<!--wp-html-compression no compression-->')
                 {
                     $overriding = !$overriding;

                     // Don't print the comment
                     continue;
                 }
                 else if ($this->remove_comments)
                 {
                     if (!$overriding && $raw_tag != 'textarea')
                     {
                         // Remove any HTML comments, except MSIE conditional comments
                         $content = preg_replace('/<!--(?!\s*(?:\[if [^\]]+]|<!|>))(?:(?!-->).)*-->/s', '', $content);
                     }
                 }
             }
             else
             {
                 if ($tag == 'pre' || $tag == 'textarea')
                 {
                     $raw_tag = $tag;
                 }
                 else if ($tag == '/pre' || $tag == '/textarea')
                 {
                     $raw_tag = false;
                 }
                 else
                 {
                     if ($raw_tag || $overriding)
                     {
                         $strip = false;
                     }
                     else
                     {
                         $strip = true;

                         // Remove any empty attributes, except:
                         // action, alt, content, src
                         $content = preg_replace('/(\s+)(\w++(?<!\baction|\balt|\bcontent|\bsrc)="")/', '$1', $content);

                         // Remove any space before the end of self-closing XHTML tags
                         // JavaScript excluded
                         $content = str_replace(' />', '/>', $content);
                     }
                 }
             }

             if ($strip)
             {
                 $content = $this->removeWhiteSpace($content);
             }

             $html .= $content;
         }

         return $html;
        }

        public function parseHTML($html)
        {
         $this->html = $this->minifyHTML($html);

         if ($this->info_comment)
         {
             $this->html .= "\n" . $this->bottomComment($html, $this->html);
         }
        }

        protected function removeWhiteSpace($str)
        {
         $str = str_replace("\t", ' ', $str);
         $str = str_replace("\n",  '', $str);
         $str = str_replace("\r",  '', $str);

         while (stristr($str, '  '))
         {
             $str = str_replace('  ', ' ', $str);
         }

         return $str;
        }
    }

    function wp_html_compression_finish($html)
    {
        return new WP_HTML_Compression($html);
    }

    function wp_html_compression_start()
    {
        ob_start('wp_html_compression_finish');
    }
    add_action('get_header', 'wp_html_compression_start');

    /**
     * Remove query string
    */ 

    function _remove_script_version( $src ){
        $parts = explode( '?ver', $src );
        return $parts[0];
    }
    add_filter( 'script_loader_src', '_remove_script_version', 15, 1 );
    add_filter( 'style_loader_src', '_remove_script_version', 15, 1 );


    /**
     * Async load
    */ 

    function PubAsync_async_scripts($url)
    {
        if ( strpos( $url, '#asyncload') === false )
            return $url;
        else if ( is_admin() )
            return str_replace( '#asyncload', '', $url );
        else
        return str_replace( '#asyncload', '', $url )."' async='async"; 
        }
    add_filter( 'clean_url', 'PubAsync_async_scripts', 11, 1 );
    
    /**
     * Team member single (Investigate following function)
    */ 
    add_filter( 'template_include', 'include_template_function', 1 );
    function include_template_function( $template_path ) {
        if ( get_post_type() == 'team_member' ) {
            if ( is_single() ) {
                if ( $theme_file = locate_template( array ( 'single-team_member.php' ) ) ) {
                    $template_path = $theme_file;
                } else {
                    $template_path = plugin_dir_path( __FILE__ ) . 'templates/single-team_member.php';
                }
            }
        }
        return $template_path;
    }

    /**
    * Move Yoast box to bottom
    */ 
    function yoasttobottom() {
        return 'low';
    }
    add_filter( 'wpseo_metabox_prio', 'yoasttobottom');

    /**
    * Redirect single page for team menber
    */ 
    add_action( 'template_redirect', 'Remove_Single_Team_Member_Page' );

    function Remove_Single_Team_Member_Page() {
    $queried_post_type = get_query_var('post_type');
        if ( is_single() && 'team_member' ==  $queried_post_type ) {
            wp_redirect( home_url(), 301 );
            exit;
        }
    }

    /**
    * Redirect Posts attachement to their Posts
    */
    function myprefix_redirect_attachment_page() {
        if ( is_attachment() ) {
            global $post;
            if ( $post && $post->post_parent ) {
                wp_redirect( esc_url( get_permalink( $post->post_parent ) ), 301 );
                exit;
            } else {
                wp_redirect( esc_url( home_url( '/' ) ), 301 );
                exit;
            }
        }
    }
    add_action( 'template_redirect', 'myprefix_redirect_attachment_page' );



    /**
     * new menu location
     */
    function register_production_menu() {
        register_nav_menu('production-menu',__( 'Production Menu' ));
      }
    add_action( 'init', 'register_production_menu' );

    /**
     * advanced validate Contact form 7.
    */

    add_filter( 'wpcf7_validate_text', 'alphanumeric_validation_filter', 20, 2 );
    add_filter( 'wpcf7_validate_text*', 'alphanumeric_validation_filter', 20, 2 );

    function alphanumeric_validation_filter( $result, $tag ) {
        $tag = new WPCF7_Shortcode( $tag );

        /**
         * first name validation
        */
        if ( 'your-name' == $tag->name ) {
            $name_of_the_input = isset( $_POST['your-name'] ) ? trim( $_POST['your-name'] ) : '';

            if ( !preg_match('~^[\p{L}\p{Z}]+$~u',$name_of_the_input) ) {
                $result->invalidate( $tag, "Must contain only alphabetic characters." );
            }else if ( strlen($name_of_the_input) >= 15 ) {
                $result->invalidate( $tag, "Name is too long. (15 character max)" );
            }

        }


        /**
         * job validation
        */
        if ( 'your-job' == $tag->name ) {
            $name_of_the_input = isset( $_POST['your-job'] ) ? trim( $_POST['your-job'] ) : '';

            if ( !preg_match('/^[A-Za-z\x{00C0}-\x{00FF}][A-Za-z\x{00C0}-\x{00FF}\'\-]+([\ A-Za-z\x{00C0}-\x{00FF}][A-Za-z\x{00C0}-\x{00FF}\'\-]+)*/u',$name_of_the_input) ) {
                $result->invalidate( $tag, "Must contain only alphabetic characters." );
            }else if ( strlen($name_of_the_input) >= 20 ) {
                $result->invalidate( $tag, "Your job title is too long. (20 character max)" );
            }

        }

        /**
         * company validation
        */
        if ( 'your-company' == $tag->name ) {
            $name_of_the_input = isset( $_POST['your-company'] ) ? trim( $_POST['your-company'] ) : '';

            if ( !preg_match('/^[A-Za-z\x{00C0}-\x{00FF}][A-Za-z\x{00C0}-\x{00FF}\'\-]+([\ A-Za-z\x{00C0}-\x{00FF}][A-Za-z\x{00C0}-\x{00FF}\'\-]+)*/u',$name_of_the_input) ) {
                $result->invalidate( $tag, "Must contain only alphabetic characters." );
            }else if ( strlen($name_of_the_input) >= 20 ) {
                $result->invalidate( $tag, "Your Company name is too long. (20 character max)" );
            }

        }

        /**
         * phone validation
        */
        if ( 'your-phone' == $tag->name ) {
            $name_of_the_input = isset( $_POST['your-phone'] ) ? trim( $_POST['your-phone'] ) : '';

            if ( !preg_match("/^(\d{3})[ -]?(\d{3})[ -]?(\d{4})$/",$name_of_the_input) ) {
                $result->invalidate( $tag, "Your Phone Number is not Valid." );
            }else if ( strlen($name_of_the_input) >= 15 ) {
                $result->invalidate( $tag, "Your phone number is too long. (15 character max)" );
            } 

        }

        /**
         * message validation
        */
        if ( 'your-message' == $tag->name ) {
            $name_of_the_input = isset( $_POST['your-message'] ) ? trim( $_POST['your-message'] ) : '';

            if( !preg_match('/^[A-Za-z\x{00C0}-\x{00FF}][A-Za-z\x{00C0}-\x{00FF}\'\-]+([\ A-Za-z\x{00C0}-\x{00FF}][A-Za-z\x{00C0}-\x{00FF}\'\-]+)*/u',$name_of_the_input) ) {
                $result->invalidate( $tag, "Must contain only alphabetic characters." );
            }

        }

        return $result;
    }
?>