<!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <?php $jellythemes = rebirth_jellythemes_theme_options(); ?>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1">
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo get_stylesheet_directory_uri() ?>/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php echo get_stylesheet_directory_uri() ?>/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_stylesheet_directory_uri() ?>/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo get_stylesheet_directory_uri() ?>/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_stylesheet_directory_uri() ?>/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo get_stylesheet_directory_uri() ?>/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo get_stylesheet_directory_uri() ?>/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo get_stylesheet_directory_uri() ?>/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_stylesheet_directory_uri() ?>/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?php echo get_stylesheet_directory_uri() ?>/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_stylesheet_directory_uri() ?>/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php echo get_stylesheet_directory_uri() ?>/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_stylesheet_directory_uri() ?>/favicon/favicon-16x16.png">
    <link rel="manifest" href="<?php echo get_stylesheet_directory_uri() ?>/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?php echo get_stylesheet_directory_uri() ?>/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <?php wp_head(); ?>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-KWNL9SR');</script>
    <!-- End Google Tag Manager -->

    <!-- Google verification -->
    <meta name="google-site-verification" content="nTfjXpePxMTdfRfHuCFsTXe963M2S2MZzvHiurTu96c" />
    <!-- End Google verification -->

</head>
<body <?php body_class(); ?> >

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KWNL9SR"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- LOADER -->
<div id="mask">
    <div class="loader"><img src="/wp-content/themes/Publicis/images/1696064.gif" /></div>
</div>
<?php if ($jellythemes['hamb_menu']): ?>
<button id="trigger-overlay" class="dropdown-icon dropdown-icon--animate invert">
<span></span>
</button>
<?php endif; ?>

<!-- HEADER -->
<?php if ($jellythemes['hamb_menu']): ?>
<header>
    <?php else : ?>
    <header id="jHeader" class="invert">
    <?php endif; ?>
    <nav class="row navbar navbar-default">
        <div class="col-sm-12 col-md-3 navbar-header">
        <?php if (!$jellythemes['hamb_menu']): ?>
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
            <span class="sr-only"><?php esc_html_e('Toggle', 'rebirth-jellythemes'); ?></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            </button>
        <?php endif; ?>
            <a class="navbar-brand" title="Publicis Production" href="#"><img src="<?php echo get_stylesheet_directory_uri() ?>/images/demo/logo-production.png" alt="Production Company Publicis Canada " /></a>
        </div>
        <div class="col-sm-12 col-md-9 navbar-container">
        <?php if (!$jellythemes['hamb_menu']): ?>
        <?php $pid = !empty($post) ? get_post_meta($post->ID, '_rebirth_jellythemes_menu_name', true) : 0; ?>
        <?php wp_nav_menu(array(
            'container' => 'div',
            'menu_class' => 'nav navbar-nav navbar-right invert',
            'container_id' => 'navbar-rebirth',
            'container_class' => 'collapse navbar-collapse navbar-ex1-collapse',
            'theme_location' => 'main',
            'menu' => 'production-menu',
            'walker' => new rebirth_jellythemes_walker_nav_menu));  ?>
        <?php endif ?>
        </div>
    </nav>
</header>