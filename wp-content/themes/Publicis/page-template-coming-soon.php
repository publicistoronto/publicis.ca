<?php /* Template Name: Page Coming Soon*/ ?>
<?php get_header(); ?>
<?php $rebirth_jellythemes = rebirth_jellythemes_theme_options();?>
<!-- INTRO -->

<div class="intro jIntro">
    <div class="image-cover menu-bottom" style="background-image:url(/wp-content/uploads/2017/05/news_header_02.jpg);">
        <div class="vcenter text-center">
            <div class="container">
                <div class="row visible">
                    <div class="col-md-8 col-md-offset-2">
                        <div class="voffset50"></div>
                        <h1 class="post-primary-title invert">Coming Soon...</span></h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>