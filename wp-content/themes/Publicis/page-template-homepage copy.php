<?php /* Template Name: Home Page */ ?>

<?php get_header(); ?>

	<?php if (function_exists('rwmb_meta')): ?>
        <?php while ( have_posts() ) : the_post(); ?>
            <?php $images = rwmb_meta('_rebirth_jellythemes_slider_images', 'type=image', $post->ID ); ?>
            <?php foreach ($images as $image) : ?>
                <?php $slide = $image['full_url']; ?>
            <?php endforeach; ?>
            <div class="intro jIntro">
              <div class="image-cover" style="background-image: url(<?php echo esc_url($slide); ?>)">
              <div class="portfolio-filter-container">
              		<div class="welcome-message"><span class="left"></span>Welcome to<span class="right"></span></div>
					<div class="wp-title"><?php bloginfo( 'name' ); ?></div>
                    <div class="welcome-message" style="padding-bottom:30px;padding-bottom: 20px; background: #b4b4b4; padding-top: 20px; color: #595959;">Our Work</div>
                    <!-- Filters desktop start -->
					<div class="row desktop-show" style="background:#b4b4b4;">
                        <div class="col-md-12" >
                            <ul class="filters">
                                <li data-order="00" data-filter="*" class="is-checked wow fadeInUp" data-wow-delay="0.5s">
                                    <span><?php esc_html_e('filter portfolio', 'rebirth-jellythemes'); ?></span>
                                </li>
                                <?php $types = get_terms('rebirth-type', array('hide_empty'=>0)); ?>
                                <?php if ( $types && ! is_wp_error( $types ) ) : ?>
                                    <?php foreach ( $types as $type ) : ?>
                                        <li data-order="<?php echo esc_js($type->slug); ?>" data-filter=".<?php echo esc_js($type->taxonomy) . '-' . esc_js($type->slug); ?>" class="wow fadeInUp" data-wow-delay="0.8s"><?php echo $type->name; ?></li>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </ul>
                        </div>
                    </div>
              </div>
              </div>
            </div>
            <!-- INTRO -->
        <?php endwhile; ?>
    <?php endif ?>

    <!-- Portfolio section -->
    <div class="section full-width" id="portfolio">
        <?php $works = new WP_Query(array('post_type'=>'rebirth-works')); ?>
        <!-- Thumbnails desktop start -->
        <div class="thumbnails work5 desktop-show">
            <?php while ($works->have_posts()) : $works->the_post(); ?>
                <?php $big = get_post_meta(get_the_ID(), '_rebirth_jellythemes_project_big', true ); ?>
                <div <?php post_class('thumbnail ' . ($big ? '' : 'small')) ?>>
                    <?php $image = rwmb_meta('_rebirth_jellythemes_project_featured', 'type=image', $post->ID ); ?>
                    <?php if (!(empty($image))): ?>
                        <?php foreach ($image as $featured): ?>
                            <?php echo wp_get_attachment_image($featured['ID'],'rebirth_jellythemes_project_list_thumb') ?>
                        <?php endforeach ?>
                    <?php else : ?>
                        <?php the_post_thumbnail('rebirth_jellythemes_project_list_thumb'); ?>
                    <?php endif ?>
                    <div class="rollover rollover5">
                        <div class="vcenter">
                            <div class="title-project"><?php the_title(); ?></div>
                            <div class="tags-project"><?php echo get_post_meta(get_the_ID(), '_rebirth_jellythemes_project_service', true ) ?></div>
                            <div class="vc_btn3-container vc_btn3-center">
                                <a href="<?php the_permalink(); ?>" class="vc_gitem-link vc_general vc_btn3 vc_btn3-size-lg vc_btn3-shape-square vc_btn3-style-custom"><?php esc_html_e('view details', 'rebirth-jellythemes'); ?></a>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endwhile; ?>
        </div>
        <!-- Thumbnails mobile start -->
        <div id="moble-portf">
        <div id="portfolio-carousel" class="carousel slide thumbnails work5 mobile-show" data-ride="carousel">
          <div class="carousel-inner" role="listbox">
            <?php while ($works->have_posts()) : $works->the_post(); ?>
                <?php $big = get_post_meta(get_the_ID(), '_rebirth_jellythemes_project_big', true ); ?>
                <div <?php post_class('thumbnail item ' . ($big ? '' : 'small')) ?>>
                    <?php $image = rwmb_meta('_rebirth_jellythemes_project_featured', 'type=image', $post->ID ); ?>
                    <?php if (!(empty($image))): ?>
                        <?php foreach ($image as $featured): ?>
                            <?php echo wp_get_attachment_image($featured['ID'],'rebirth_jellythemes_project_list_thumb') ?>
                        <?php endforeach ?>
                    <?php else : ?>
                        <?php the_post_thumbnail('rebirth_jellythemes_project_list_thumb'); ?>
                    <?php endif ?>
                    <div class="vc_gitem-zone vc_gitem-zone-b vc_custom_1496172810748 rollover rollover5 vc-gitem-zone-height-mode-auto">
                        <div class="vc_gitem-zone-mini">
                            <div class="vc_gitem_row vc_row vc_gitem-row-position-middle">
                                <div class="vc_col-sm-12 vc_gitem-col vc_gitem-col-align-">
                                    <div class="vc_custom_heading title-project vc_gitem-post-data vc_gitem-post-data-source-post_title">
                                        <div style="text-align: center">
                                            <a href="<?php the_permalink(); ?>" class="vc_gitem-link" title="<?php the_title(); ?>"><?php the_title(); ?></a>
                                        </div>
                                    </div>
                                    <div class="vc_btn3-container vc_btn3-center">
                                        <a href="<?php the_permalink(); ?>" class="vc_gitem-link vc_general vc_btn3 vc_btn3-size-lg vc_btn3-shape-square vc_btn3-style-custom" title="View details" style="background-color:rgb(215,33,48);background-color:rgba(215,33,48,0.1);color:#ffffff;"><?php esc_html_e('view details', 'rebirth-jellythemes'); ?></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endwhile; ?>
          </div>
          <a class="carousel-control prev fa fa-angle-left" href="#portfolio-carousel" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="carousel-control next fa fa-angle-right" href="#portfolio-carousel" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>
        </div>
    </div>
   
    <!-- Content area -->
    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        <?php the_content(); ?>
    <?php endwhile; ?>
    <?php endif; ?>
   
    <!-- Team members section -->
    <div class="section full-width" id="team-members-container">
        <?php 
            $members = new WP_Query(array('post_type'=>'team_member')); 
            $members2 = array_chunk($members->posts, 4, true);

            $tmb = array();
            $nam = array();
            $pos = array();
            $social = array();
            $socialEach = array();
            while ($members->have_posts()) : $members->the_post();
                $team_thumb = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), 'full' );
                $position = get_post_meta(get_the_ID(), 'team_member_position', true);
                $teamMemberName = $title_text = apply_filters( 'team_grid_filter_title', get_the_title() );	
                $team_member_social_links = get_post_meta( get_the_ID(), 'team_member_social_links', true );
                $tmb[] = $team_thumb[0];
                array_push($pos, $position);
                array_push($nam, $teamMemberName);
                $social[] = $team_member_social_links;
            endwhile;
        ?>
        <div style=""> 
            <div id="members-carousel2" class="carousel slide thumbnails work5 desktop-show" data-ride="carousel" style="height:auto !important;    max-width: none;     position: relative;">
                <div class="carousel-inner" role="listbox" style="height:500px;">
                    <?php
                    for ($i = 0; $i < sizeof($members2); $i++) {    
                       echo '<div class="item" style="height: 500px;">';
                        for ($a = 0; $a < sizeof($members2[$i]); $a++) {
                            echo '<div class="thumbnail col-md-3">
                            <div class="member-image" style="background-image: url(' .$tmb[0]. ') ;    background-size: cover;background-position: 40% 50%;min-height: 350px; "></div>
                            <div class="rollover rollover5">
                            <div class="vcenter">
                            <div class="member-name">'.$nam[0].'
                            </div>
                            <div class="member-position">'.$pos[0].'
                            </div>
                            <div class="member-social">
                            <div class="team-meamber-single">
                            <div class="team-social ">';
                            foreach ($social[0] as $motKey => $mot) { 
                                if ($mot == true) {
                                    echo '<span class="'.$motKey.'">
                                            <a target="_blank" href="'.$mot.'"> </a>
                                    </span>';
                                }  
                            } 
                            echo '</div></div></div></div></div></div>';
                            array_shift($tmb);
                            array_shift($pos);
                            array_shift($nam);
                            array_shift($social);

}
                       echo '</div>';
                    }
                    ?>
                </div>
                <a class="carousel-control prev fa fa-angle-left" href="#members-carousel2" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control next fa fa-angle-right" href="#members-carousel2" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
           </div>
        </div>
        <!-- Thumbnails mobile start -->
        <div id="moble-team"> 
            <div id="members-carousel" class="carousel slide thumbnails work5 mobile-show" data-ride="carousel">
                  <div class="carousel-inner" role="listbox">
                    <?php while ($members->have_posts()) : $members->the_post(); ?>
                        <div class="thumbnail item col-md-3">
                        <div class="member-image" style="background-image: url(<?php the_post_thumbnail_url( $size ); ?>) "></div>
                        <div class="rollover rollover5">
                            <div class="vcenter">
                                <div class="member-name"><?php the_title(); ?></div>
                                <div class="member-position"><?php echo get_post_meta($post -> ID, 'team_member_position', true); ?></div>
                                <?php $team_member_social_links = get_post_meta( get_the_ID(), 'team_member_social_links', true ); ?>
                                <div class="member-social">
                                    <?php $html = ''; $html.= '<div class="team-meamber-single">'; include team_plugin_dir.'/templates/team-grid-social.php'; $html.= '</div>'; ?>
                                    <?php echo $html; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php endwhile; ?>
                  </div>
                  <a class="carousel-control prev fa fa-angle-left" href="#members-carousel" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                  </a>
                  <a class="carousel-control next fa fa-angle-right" href="#members-carousel" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                  </a>

            </div>
        </div>
        <div class="voffset50"></div>
   </div>
<?php get_footer(); ?>