<?php /* Template Name: Home Page */ ?>

<?php get_header(); ?>

	<?php if (function_exists('rwmb_meta')): ?>
        <?php while ( have_posts() ) : the_post(); ?>
            <?php $images = rwmb_meta('_rebirth_jellythemes_slider_images', 'type=image', $post->ID ); ?>
            <?php foreach ($images as $image) : ?>
                <?php $slide = $image['full_url']; ?>
            <?php endforeach; ?>
            <div class="intro jIntro">
              <div class="image-cover" style="background-image: url(<?php echo esc_url($slide); ?>)">
                <div class="portfolio-filter-container">
                <div class="welcome-message"><span class="left"></span>Welcome to<span class="right"></span></div>
                <div class="wp-title"><?php bloginfo( 'name' ); ?></div>
                </div>
              </div>
            </div>
            <!-- INTRO -->
        <?php endwhile; ?>
    <?php endif ?>

    <!-- Portfolio section -->
    <div class="section full-width" id="portfolio">
        <?php
            $works = new WP_Query(
                array(
                    'post_type'=>'rebirth-works', 
                    'posts_per_page'=> '8',
                    'orderby'   => 'rand',
                    'meta_query' => array(
                        array(
                            'key' => 'featured_work',
                            'compare' => '==',
                            'value' => '1'
                        )
                    )
                )
            ); 
            if( $works ): 
        ?>
        <!-- Thumbnails desktop start -->
        <div class="thumbnails work5 desktop-show">
            <?php while ($works->have_posts()) : $works->the_post(); ?>
                <?php $big = get_post_meta(get_the_ID(), '_rebirth_jellythemes_project_big', true ); ?>
                <div <?php post_class('thumbnail ' . ($big ? '' : 'small')) ?>>
                    <?php $image = rwmb_meta('_rebirth_jellythemes_project_featured', 'type=image', $post->ID ); ?>
                    <?php if (!(empty($image))): ?>
                        <?php foreach ($image as $featured): ?>
                            <?php echo wp_get_attachment_image($featured['ID'],'rebirth_jellythemes_project_list_thumb') ?>
                        <?php endforeach ?>
                    <?php else : ?>
                        <?php the_post_thumbnail('rebirth_jellythemes_project_list_thumb'); ?>
                    <?php endif ?>
                    
                    <div class="rollover rollover5">
                    <a href="<?php the_permalink(); ?>" class="anchor-full"></a>
                            <div class="vcenter">
                                <div class="title-project"><?php the_title(); ?></div>
                                <div class="tags-project"><?php echo get_post_meta(get_the_ID(), '_rebirth_jellythemes_project_service', true ) ?></div>
                                <div class="vc_btn3-container vc_btn3-center">
                                    <a href="<?php the_permalink(); ?>" class="vc_gitem-link vc_general vc_btn3 vc_btn3-size-lg vc_btn3-shape-square vc_btn3-style-custom"><?php esc_html_e('view details', 'rebirth-jellythemes'); ?></a>
                                </div>
                            </div>
                            
                    </div>
                    
                </div>
            <?php endwhile; ?>
        </div>
        <!-- Thumbnails mobile start -->
        <div id="moble-portf">
        <div id="portfolio-carousel" class="carousel slide thumbnails work5 mobile-show" data-ride="carousel">
          <div class="carousel-inner" role="listbox">
            <?php while ($works->have_posts()) : $works->the_post(); ?>
                <?php $big = get_post_meta(get_the_ID(), '_rebirth_jellythemes_project_big', true ); ?>
                <div <?php post_class('thumbnail item ' . ($big ? '' : 'small')) ?>>
                    <?php $image = rwmb_meta('_rebirth_jellythemes_project_featured', 'type=image', $post->ID ); ?>
                    <?php if (!(empty($image))): ?>
                        <?php foreach ($image as $featured): ?>
                            <?php echo wp_get_attachment_image($featured['ID'],'rebirth_jellythemes_project_list_thumb') ?>
                        <?php endforeach ?>
                    <?php else : ?>
                        <?php the_post_thumbnail('rebirth_jellythemes_project_list_thumb'); ?>
                    <?php endif ?>
                    <div class="vc_gitem-zone vc_gitem-zone-b vc_custom_1496172810748 rollover rollover5 vc-gitem-zone-height-mode-auto">
                        <div class="vc_gitem-zone-mini">
                            <div class="vc_gitem_row vc_row vc_gitem-row-position-middle">
                                <div class="vc_col-sm-12 vc_gitem-col vc_gitem-col-align-">
                                    <div class="vc_custom_heading title-project vc_gitem-post-data vc_gitem-post-data-source-post_title">
                                        <div style="text-align: center">
                                            <a href="<?php the_permalink(); ?>" class="vc_gitem-link" title="<?php the_title(); ?>"><?php the_title(); ?></a>
                                        </div>
                                    </div>
                                    <div class="vc_btn3-container vc_btn3-center">
                                        <a href="<?php the_permalink(); ?>" class="vc_gitem-link vc_general vc_btn3 vc_btn3-size-lg vc_btn3-shape-square vc_btn3-style-custom" title="View details" style="background-color:rgb(215,33,48);background-color:rgba(215,33,48,0.1);color:#ffffff;"><?php esc_html_e('view details', 'rebirth-jellythemes'); ?></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endwhile; ?>
          </div>
          <a class="carousel-control prev fa fa-angle-left" href="#portfolio-carousel" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="carousel-control next fa fa-angle-right" href="#portfolio-carousel" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>
        </div>
        <?php endif; ?>
    </div>
   
    <!-- Content area -->
    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        <?php the_content(); ?>
    <?php endwhile; ?>
    <?php endif; ?>
   
<?php get_footer(); ?>