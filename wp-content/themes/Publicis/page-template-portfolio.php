<?php /* Template Name: Page Portfolio */ ?>
<?php get_header(); ?>
    <?php if (function_exists('rwmb_meta')): ?>
        <?php while ( have_posts() ) : the_post(); ?>
            <?php $images = rwmb_meta('_rebirth_jellythemes_portfolio_images', 'type=image', $post->ID ); ?>
            <section class="intro full-width jIntro" id="anchor00">
                <div class="container-full">
                    <div class="row row-no-gutter">
                        <div class="col-md-12">
                            <div class="slider-intro">
                                <div id="slides">
                                    <div class="slides-container">
                                        <?php foreach ($images as $image) : ?>
                                            <?php $slide = $image['full_url']; ?>
                                            <img src="<?php echo esc_url($slide); ?>">
                                        <?php endforeach; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="vcenter text-center text-overlay">
                        <h1 class="primary-title invert">
                            <strong class="work">
                                <?php $text =  get_post_meta( $post->ID, '_rebirth_jellythemes_portfolio_text', true ); ?>
                                <?php echo wp_kses($text, array(
                                    'strong' => array(),
                                    'span' => array('class' => array()),
                                    'br' => array())); 
                                ?>
                            </strong>
                        </h1>
                        <div class="voffset10"></div>
                        <?php $texts =  get_post_meta( $post->ID, '_rebirth_jellythemes_portfolio_button_1', true ); ?>
                        <?php $links =  get_post_meta( $post->ID, '_rebirth_jellythemes_portfolio_button_link_1', true ); ?>
                        <?php if (!empty($texts)): ?>
                            <ul class="list-horizontal-links invert">
                                <?php foreach ($texts as $key => $value): ?>
                                    <?php if (!empty($links[$key]) && !empty($value)): ?>
                                        <li><a href="<?php echo esc_url($links[$key]) ?>"><?php echo  wp_kses($value, array());  ?></a></li>
                                    <?php endif ?>
                                <?php endforeach ?>
                            </ul>
                        <?php endif ?>
                    </div>
                </div>
            </section>
            <!-- INTRO -->
        <?php endwhile; ?>
        <?php $back = $post //backup post data ?>
        <?php get_template_part('templates/loop', 'portfolio'); ?>
        <?php $post = $back //restore post data ?>
        <?php get_template_part('templates/loop', 'pages'); ?>
        <?php $post = $back //restore post data ?>
    <?php endif ?>
<?php get_footer(); ?>
