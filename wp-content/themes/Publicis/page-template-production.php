<?php /* Template Name: Production */ ?>

<?php get_header('production'); ?>

<?php if (function_exists('rwmb_meta')): ?>
    <?php while ( have_posts() ) : the_post(); ?>
        <?php $images = rwmb_meta('_rebirth_jellythemes_slider_images', 'type=image', $post->ID ); ?>
        <?php $texts =  get_post_meta( $post->ID, '_rebirth_jellythemes_slider_text', true ); ?>
        <?php foreach ($images as $image) : ?>
            <?php $slide = $image['full_url']; ?>
        <?php endforeach; ?>
        <div id="pinContainer">
            <div class="intro jIntro production">
                <div class="image-cover" style="background-image: url(<?php echo esc_url($slide); ?>)">
                    <?php foreach ($texts as $i => $text) : ?>
                        <div class="slider-text-container">
                            <div class="slider-text-content">
                                <h1><?php echo wp_kses($text, array('strong' => array())); ?></h1>
                            </div>
                            <div class="iconScroll invert">
                                <div class="scroll__down">
                                    <span class="scroll__mouse">
                                        <span class="scroll__wheel"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                    <div class="production-heading-container">
                    <section class="panel left">
                        <div class="image-container left">
                            <div class="image-content">
                                <div class="text-container left">
                                    <p>High Quality</p>
                                </div>
                                <img src="<?php echo get_stylesheet_directory_uri() ?>/images/demo/and-sign-left.png" alt="High Quality" />
                            </div>
                        </div>
                    </section>
                    <section class="panel right">
                        <div class="image-container right">
                            <div class="image-content">
                                <div class="text-container right">
                                    <p>Low Costs</p>
                                </div>
                                <img src="<?php echo get_stylesheet_directory_uri() ?>/images/demo/and-sign-right.png" alt="Low Costs" />
                            </div>
                        </div>
                    </section>
                    </div>
                </div>
            </div>
        </div>
        <!-- INTRO -->
    <?php endwhile; ?>
<?php endif ?>

<!-- Content area -->
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    <?php the_content(); ?>
<?php endwhile; ?>
<?php endif; ?>

<?php get_footer('production'); ?>



