<?php /* Template Name: Page Photo Header */ ?>
<?php get_header(); ?>
    <?php if (function_exists('rwmb_meta')): ?>
        <?php while ( have_posts() ) : the_post(); ?>
            <?php $images = rwmb_meta('_rebirth_jellythemes_slider_images', 'type=image', $post->ID ); ?>
            <?php foreach ($images as $image) : ?>
                <?php $slide = $image['full_url']; ?>
            <?php endforeach; ?>
            <div class="intro jIntro">
              <div class="image-cover" style="background-image: url(<?php echo esc_url($slide); ?>)">
                <div class="iconScroll invert">
                  <h1 class="info"><?php the_title(); ?></h1>
                  <div class="icon">
                    <i class="ico-expression scroll"></i>
                  </div>
                </div>
              </div>
            </div>
            <!-- INTRO -->
        <?php endwhile; ?>
        <?php $back = $post //backup post data?>
        <?php get_template_part('templates/loop', 'pages'); ?>
        <?php $post = $back //restore post data ?>
    <?php endif ?>
    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        <?php the_content(); ?>
    <?php endwhile; ?>
    <?php endif; ?>
<?php get_footer(); ?>
