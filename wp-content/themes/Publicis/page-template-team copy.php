<?php /* Template Name: DEV: Our Team Template */ ?>
<?php get_header(); ?>
<?php
    $loop = new WP_Query( array( 'post_type' => 'team_member') );
    if ( $loop->have_posts() ) :
        while ( $loop->have_posts() ) : $loop->the_post(); ?>
            <div class="team-member-slider">
                <?php if ( has_post_thumbnail() ) { ?>
                    <div class="col-md-2">
                        <a href="<?php the_permalink(); ?>">
                            <?php the_post_thumbnail(); ?>
                            <svg height="0" width="0">
                            <defs>
                                <clipPath id="cross">
                                <rect y="110" x="137" width="90" height="90"/>
                                <rect x="0" y="110" width="90" height="90"/>
                                <rect x="137" y="0" width="90" height="90"/>
                                <rect x="0" y="0" width="90" height="90"/>
                                </clipPath>
                            </defs>
                            </svg>
                        </a>
                    </div>
                <?php } ?>
                
            </div>
        <?php endwhile;
    endif;
    wp_reset_postdata();
?>
<?php get_footer(); ?>