<?php /* Template Name: Our Team Template */ ?>
<?php get_header(); ?>
    <!-- Team members section -->
    <div class="section full-width" id="team-members-container">
        <?php 
            $members = new WP_Query(array('post_type'=>'team_member', 'orderby' => 'rand', 'posts_per_page' => 8)); 
            $members2 = array_chunk($members->posts, 4, true);

            $tmb = array();
            $nam = array();
            $pos = array();
            $social = array();
            $socialEach = array();
            while ($members->have_posts()) : $members->the_post();
                $team_thumb = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), 'full' );
                $team_permalink = get_permalink();
                $position = get_post_meta(get_the_ID(), 'team_member_position', true);
                $teamMemberName = $title_text = apply_filters( 'team_grid_filter_title', get_the_title() );	
                $team_member_social_links = get_post_meta( get_the_ID(), 'team_member_social_links', true );
                $tmb[] = $team_thumb[0];
                $link[] = $team_permalink;
                array_push($pos, $position);
                array_push($nam, $teamMemberName);
                $social[] = $team_member_social_links;
            endwhile;
        ?>
        <div class="wrapper"> 
            <div id="members-carousel2" class="carousel slide thumbnails work5 desktop-show" data-ride="carousel" style="height:auto !important;    max-width: none;     position: relative;">
                <div class="carousel-inner" role="listbox" style="height:500px;">
                    <?php
                    for ($i = 0; $i < sizeof($members2); $i++) {    
                       echo '<div class="item" style="height: 500px;">';
                        for ($a = 0; $a < sizeof($members2[$i]); $a++) {
                            echo '<div class="thumbnail col-md-3">
                            <div class="member-image" style="background-image: url(' .$tmb[0]. ') ;    background-size: cover;background-position: 40% 50%;min-height: 350px; "></div>
                            <div class="rollover rollover5">
                            <a href="'.$link[0].'" class="anchor-full"></a>
                            <div class="vcenter">
                            <div class="member-name">'.$nam[0].'
                            </div>
                            <div class="member-position">'.$pos[0].'
                            </div>
                            <div class="member-social">
                            <div class="team-meamber-single">
                            <div class="team-social ">';
                            foreach ($social[0] as $motKey => $mot) { 
                                if ($mot == true) {
                                    echo '<span class="'.$motKey.'">
                                            <a target="_blank" href="'.$mot.'"> </a>
                                    </span>';
                                }  
                            } 
                            echo '</div></div></div></div></div></div>';
                            array_shift($link);
                            array_shift($tmb);
                            array_shift($pos);
                            array_shift($nam);
                            array_shift($social);
                        }
                       echo '</div>';
                    }
                    ?>
                </div>
                <a class="carousel-control prev fa fa-angle-left" href="#members-carousel2" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control next fa fa-angle-right" href="#members-carousel2" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
           </div>
        </div>
        <!-- Thumbnails mobile start -->
        <div id="moble-team"> 
            <div id="members-carousel" class="carousel slide thumbnails work5 mobile-show" data-ride="carousel">
                  <div class="carousel-inner" role="listbox">
                    <?php while ($members->have_posts()) : $members->the_post(); ?>
                        <div class="thumbnail item col-md-3">
                        <div class="member-image" style="background-image: url(<?php the_post_thumbnail_url( $size ); ?>) "></div>
                        <div class="rollover rollover5">
                            <a href="<?php the_permalink(); ?>" class="anchor-full"></a>
                            <div class="vcenter">
                                <div class="member-name"><?php the_title(); ?></div>
                                <div class="member-position"><?php echo get_post_meta($post -> ID, 'team_member_position', true); ?></div>
                                <?php $team_member_social_links = get_post_meta( get_the_ID(), 'team_member_social_links', true ); ?>
                                <div class="member-social">
                                    <?php $html = ''; $html.= '<div class="team-meamber-single">'; include team_plugin_dir.'/templates/team-grid-social.php'; $html.= '</div>'; ?>
                                    <?php echo $html; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php endwhile; ?>
                  </div>
                  <a class="carousel-control prev fa fa-angle-left" href="#members-carousel" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                  </a>
                  <a class="carousel-control next fa fa-angle-right" href="#members-carousel" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                  </a>

            </div>
        </div>
   </div>
    <!-- Content area -->
    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        <?php the_content(); ?>
    <?php endwhile; ?>
    <?php endif; ?>
<?php get_footer(); ?>