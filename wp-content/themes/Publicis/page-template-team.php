<?php /* Template Name: Our Team Template */ ?>
<?php get_header(); ?>
    <?php if (function_exists('rwmb_meta')): ?>
        <?php while ( have_posts() ) : the_post(); ?>
            <?php $images = rwmb_meta('_rebirth_jellythemes_slider_images', 'type=image', $post->ID ); ?>
            <?php foreach ($images as $image) : ?>
                <?php $slide = $image['full_url']; ?>
            <?php endforeach; ?>
            <div class="intro jIntro">
              <div class="image-cover" style="background-image: url(<?php echo esc_url($slide); ?>)">
                <div class="iconScroll invert">
                  <h1 class="info">WE ARE LIONHEARTED</h1>
                  <div class="icon">
                    <i class="ico-expression scroll"></i>
                  </div>
                </div>
              </div>
            </div>
            <!-- INTRO -->
        <?php endwhile; ?>
        <?php $back = $post //backup post data?>
        <?php get_template_part('templates/loop', 'pages'); ?>
        <?php $post = $back //restore post data ?>
    <?php endif ?>
    <!-- Content area -->
    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        <?php the_content(); ?>
    <?php endwhile; ?>
    <?php endif; ?>
    <?php
        // The Query
        $the_query = new WP_Query( 
            array(  'post_type'=>'team_member', 
                    'orderby' => 'rand',
                    'tax_query' => array(
                        array(
                            'taxonomy' => 'team_group',
                            'field' => 'slug',
                            'terms' => 'lion-hearted-carousel',
                            'operator' => 'IN'
                        )
                    )
            )
        );
        // The Loop
        ?>
            <div id="lionHeartedCarousel" class="carousel slide" data-ride="carousel" data-interval="5000">
                <div class="carousel-inner">
                    <?php the_post(); ?>
                    <?php
                    if ( $the_query->have_posts() ) {
                    while ( $the_query->have_posts() ) {
                        $the_query->the_post();
                    ?>
                        <div class="item" style="background-image: url('<?php the_post_thumbnail_url(); ?>')">
                            <div class="lionImageHolder col-xs-12">
                                <img src="<?php the_post_thumbnail_url(); ?>" alt="<?php the_title(); ?>"/>
                            </div>
                            <div class="lionInfoHolder col-xs-12 col-md-6 ">
                                <p class="lionName"><?php the_title(); ?></p>
                                <p class="lionRole"><?php echo get_post_meta(get_the_ID(), 'team_member_position', true); ?></p>
                                <div class="lionDesc"><?php the_content(); ?></div>
                            </div>
                        </div>
                    <?php
                    }
                    wp_reset_postdata();
                    ?>
                </div>
                <a class="tp-leftarrow tparrows hesperiden" href="#lionHeartedCarousel" role="button" data-slide="prev">
                    <span class="" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="tp-rightarrow tparrows hesperiden" href="#lionHeartedCarousel" role="button" data-slide="next">
                    <span class="" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        <?php
        } else {
            // no posts found
        }
    ?>
<?php get_footer(); ?>