<?php /* Template Name: Page Video Slider */ ?>
<?php get_header(); ?>
    <?php if (function_exists('rwmb_meta')): ?>
        <?php while ( have_posts() ) : the_post(); ?>
            <?php $revslider = get_post_meta( $post->ID, '_rebirth_jellythemes_revslider', true ); ?>
            <?php if (!empty($revslider)): ?>
                <?php echo do_shortcode($revslider); ?>
            <?php else : ?>
                <?php $video = get_post_meta( $post->ID, '_rebirth_jellythemes_slider_video', true ); ?>
                <?php $images = rwmb_meta('_rebirth_jellythemes_slider_images', 'type=image', $post->ID ); ?>
                <?php $images = array_values($images); $image = array_shift($images); ?>

                <section class="intro full-width jIntro" style="background:url('<?php echo esc_url($image['full_url']); ?>') center center !important;" id="<?php echo esc_attr($post->post_name); ?>">

                    <div class="playerVideo" data-property="{videoURL:'<?php echo esc_js($video); ?>',showControls:false,containment:'self',autoPlay:true, mute:false, startAt:0,opacity:1,ratio:'4/3', addRaster:true}"> <!-- Slider -->
                        <a href="#" class="play-video playing"><?php esc_html_e('Play/Pause', 'rebirth-jellythemes'); ?></a>
                    </div>

                    <div class="vcenter text-center text-overlay">
                        <?php $texts =  get_post_meta( $post->ID, '_rebirth_jellythemes_slider_text', true ); ?>
                        <div id="owl-main-text" class="owl-carousel">
                            <?php foreach ($texts as $i => $text) : ?>
                                <div class="item">
                                    <h1 class="primary-title invert"><?php echo wp_kses($text, array('strong' => array())); ?></h1>
                                </div>
                            <?php endforeach; ?>
                        </div>
                        <div class="voffset20"></div>
                        <a href="<?php echo esc_url(get_post_meta( $post->ID, '_rebirth_jellythemes_slider_button_link_1', true )); ?>" class="btn btn-invert"><?php echo get_post_meta( $post->ID, '_rebirth_jellythemes_slider_button_1', true ); ?></a>
                    </div>
                </section>
            <?php endif; ?>
        <?php endwhile; ?>
        <?php $back = $post //backup post data?>
        <?php get_template_part('templates/loop', 'pages'); ?>
        <?php $post = $back //restore post data ?>
    <?php endif ?>
<?php get_footer(); ?>
