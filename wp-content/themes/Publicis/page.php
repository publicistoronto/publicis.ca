<?php get_header(); ?>
	<div class="section blog single-post" id="anchor07">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<article <?php post_class('post-details'); ?>>
							<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
							<div class="voffset140"></div>
							<h1 class="title post-detail"><?php the_title(); ?></h1>

							<?php the_content(); ?>
		                    <?php wp_link_pages(); ?>

							<div class="voffset60"></div>
						<?php endwhile; ?>
						<?php endif; ?>
						<?php comments_template(); ?>
					</article>
				</div>
			</div>
		</div>
	</div>

<?php get_footer(); ?>
