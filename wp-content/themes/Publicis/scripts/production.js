(function ($) {
	'use strict';
	// global variables
    var windowWidth = $(window).width();
	var thumbsheight, tworowthumbheight, visibleli, onerowthumbheight;
	
    $(window).bind("load", function () {
        setTimeout(function () {

			// remove mask on load 
			$("#mask").delay(500).fadeOut();

			// create home page carousel
            $('#moble-portf .carousel-inner .item:first-child').addClass("active");
            $('#moble-team .carousel-inner .item:first-child').addClass("active");
            $('#members-carousel2 .carousel-inner .item:first-child').addClass("active");
            $('#moble-portf #portfolio-carousel .item').removeAttr('style');
            thumbsheight = $("#portfolio .thumbnails.desktop-show .thumbnail").height();
            tworowthumbheight = thumbsheight * 2 + "px";
            $("#portfolio .thumbnails.desktop-show").removeAttr("style");
            $("#portfolio .thumbnails.desktop-show").attr("style", "position:relative; height:" + tworowthumbheight + " ");
            $("#team-members-container .thumbnail").removeAttr("style");
			$("#moble-portf #portfolio-carousel .carousel-inner").css("height", windowWidth);
			
			// scroll to clients section when url location is #clients-container
			if (window.location.href.indexOf('/#clients-container') > -1) {
				$('html').animate({
					scrollTop: $('#clients-container').offset().top - 100
				}, 700);
			}

        }, 500);
	});

	// resize event for portfolio items alignment
	$(window).resize(function() {
		clearTimeout(window.resizedFinished);
		window.resizedFinished = setTimeout(function () {
			if ($(window).width() >= 768){
				$('#moble-portf #portfolio-carousel .item').removeAttr('style');
				thumbsheight = $("#portfolio .thumbnails.desktop-show .thumbnail").height();
				tworowthumbheight = thumbsheight * 2 + "px";
				$("#portfolio .thumbnails.desktop-show").removeAttr("style");
				$("#portfolio .thumbnails.desktop-show").attr("style", "position:relative; height:" + tworowthumbheight + " ");
				$("#team-members-container .thumbnail").removeAttr("style");
				$("#moble-portf #portfolio-carousel .carousel-inner").css("height", windowWidth);
			}else if ($(window).width() < 768){
				$('#moble-portf #portfolio-carousel .item').removeAttr('style');
				thumbsheight = $("#portfolio .thumbnails.mobile-show .thumbnail").height();
				onerowthumbheight = thumbsheight  + "px";
				$("#portfolio .thumbnails.mobile-show").removeAttr("style");
				$("#portfolio .thumbnails.mobile-show").attr("style", "position:relative; height:" + onerowthumbheight + " ");
				$("#team-members-container .thumbnail").removeAttr("style");
				$("#portfolio-carousel .carousel-inner").attr("style", "height:" + onerowthumbheight + " ");
			}else{
				return false;
			}
		}, 500);
    });

	$(document).ready(function () {
        $(".home .clients-drpd").addClass("dropdown-toggle");
        $(".home .clients-drpd").attr("data-toggle", "dropdown");
		$(".home .clients-inline").addClass("dropdown-menu");
		if ($(window).width() > 768) {
			$(".home #clients-container .wpb_wrapper:first-child").addClass("open");
			$(".home .clients-drpd").attr("aria-expanded","true");
		} else {
			$(".home #clients-container .wpb_wrapper:first-child").removeClass("open");
			$(".home .clients-drpd").attr("aria-expanded","false");
		}

		// sorting the list items for portfolio filter
		$(".filters").each(function () {
			$(this).html($(this).children('li').sort(function (a, b) {
				return ($(b).data('order')) < ($(a).data('order')) ? 1 : -1;
			}));
		});
		
		// our work li filter
		$(".filters li").click(function () {
			setTimeout(function () {
				thumbsheight = $("#portfolio .thumbnails.desktop-show .thumbnail").height();
				visibleli = $("#portfolio .thumbnails.desktop-show .thumbnail:visible").length;
				if (visibleli <= 4) {
					$("#portfolio .thumbnails.desktop-show").removeAttr("style");
					$("#portfolio .thumbnails.desktop-show").attr("style", "position:relative; height:" + thumbsheight + "px" + " ");
				} else {
					$("#portfolio .thumbnails.desktop-show").removeAttr("style");
					$("#portfolio .thumbnails.desktop-show").attr("style", "position:relative; height:" + thumbsheight * 2 + "px" + " ");
				}
                $("#members-carousel2 .thumbnail").removeAttr("style"); 
			}, 500);
		});
		
		// fade out err messages for contact form 7
        setInterval(function () {
            $(".wpcf7-validation-errors").fadeOut();
        }, 10000);
		
		// remove active class from header links when on home page
        if ( window.location.pathname == '/' ){
            $("#navbar-rebirth ul li").removeClass("active");
        }
        
        if(window.location.href.indexOf("portfolio") > -1){
            $(".wpb_wrapper").addClass("open"); 
		}
		
		// init carousel lion hearted
		$('#lionHeartedCarousel .carousel-inner .item:first').addClass('active');
        
	});

	// click event when clients section clicked
	$('.clients-inline').click(function() {
		setTimeout(function () {
			$(".home #clients-container .wpb_wrapper:first-child").addClass("open");
			$(".home .clients-drpd").attr("aria-expanded","true");
		}, 0);
	});

	// tasks to do when user clicked on our clients link in header
	$('.menu-link[data-target="/#clients-container"]').click(function() {
		setTimeout(function () {
			$('#navbar-rebirth').removeClass('in');
			$('#navbar-rebirth').attr('aria-expanded','true');
			$('#navbar-rebirth').attr('style', '');
			$('html').animate({
				scrollTop: $('#clients-container').offset().top -100
			}, 1000);
			$(".home #clients-container .wpb_wrapper:first-child").addClass("open");
			$(".home .clients-drpd").attr("aria-expanded","true");
		}, 0);
	});
    
    // works page custom tabs fade transition behaviour.
    $(".nav-tabs li a").click(function () {
        $(".tab-content .tab-pane").removeClass("opOne");
        $(".tab-content .tab-pane").addClass("opZero");
        setInterval(function () {
            $(".tab-content .tab-pane.active").removeClass("opZero");
            $(".tab-content .tab-pane.active").addClass("opOne");
        }, 300);
	});

	// pause lion-hearted carousel on mobile when touch
	$('#lionHeartedCarousel .carousel-inner').click(function () {
		$('#lionHeartedCarousel').carousel('pause');
		console.log('div clicked');
	});

	// resume lion-hearted carousel which click next/prev
	$('.tparrows').click(function () {
		$('#lionHeartedCarousel').carousel('cycle');
	}); 
      
})(jQuery);


(function ($) {
	'use strict';
	// scroll magic init
	$(function () { // wait for document ready
		// init
		var controller = new ScrollMagic.Controller();

		// define movement of panels
		var wipeAnimation = new TimelineMax()
			.fromTo("section.panel.left", 1, {x: "-100%"}, {x: "0%", ease: Linear.easeNone}, 0)  // in from left
			.fromTo("section.panel.right", 1, {x:  "100%"}, {x: "0%", ease: Linear.easeNone}, 0);

		// create scene to pin and link animation
		new ScrollMagic.Scene({
			triggerElement: "#pinContainer",
			triggerHook: 'onLeave',
			duration: "350"
		})
		.setPin("#pinContainer")
		.setTween(wipeAnimation)
		.addTo(controller);
	});

	// watch user idle for recaptcha view state
	var idleState = false;
	var idleDuration = null;
	$('*').bind('click keydown keypress keyup submit resize dblclick', function () {
		if($('body').hasClass('page-template-page-template-production')){
			clearTimeout(idleDuration);
			if (idleState == true) {
				$('.grecaptcha-badge').fadeIn();
			}
			idleState = false;
			idleDuration = setTimeout(function () {
				if($('.grecaptcha-badge').attr('style')){
					$('.grecaptcha-badge').fadeOut();
				} else{
					$('.grecaptcha-badge').fadeIn();
				}
				idleState = true; 
			}, 3000);
		}
	
	});
	$("body").trigger("mousemove");

	// adjust document scrolling position when clicking menu
	$('#menu-production-menu .main-menu-link').on('click', function(e){ 
		e.preventDefault();

		var id = $(this).data('target').split('/').pop();
		var top = $(id).offset().top;
		var offset = document.getElementById('jHeader').getBoundingClientRect().height;

		$('body, html').animate({scrollTop: top - offset}, 0);
	});

	// adjust document scrolling position when clicking tab module CTA
	$('.tab-sections .section-cta .vc_btn3').on('click', function (e) {
		e.preventDefault();

		var id = $(this).attr("href").split('/').pop();
		var top = $(id).offset().top;
		var offset = document.getElementById('jHeader').getBoundingClientRect().height;

		$('body, html').animate({ scrollTop: top - offset }, 0);
	});
	
     
	// dropdown menu for tabs-section in Diigtal Module
	$(function () {
		$('.tab-sections .vc_tta-tabs-container').on('click', function () {
			$(this).toggleClass('dropdown-visible'); 
		});
	});



})(jQuery);