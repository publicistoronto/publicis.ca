<?php /* Template Name: Single Team Template */ ?>
<?php get_header(); ?>
	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
	<?php
		$thumbnail_data = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'my-fun-size' );
		$thumbnail_url = $thumbnail_data[0];
		$position = get_post_meta(get_the_ID(), 'team_member_position', true);
	?>
	<div class="vc_row row wpb_row jt_row vc_row row-fluid jt_row-fluid visible team-wrapper">
		<div class="team-container">
			<div class="pb-contact-map wpb_column jt_col vc_column_container vc_col-sm-6 col-sm-6">
				<div class="vc_column-inner ">
					<div class="wpb_wrapper responsive" style="background-image: url('<?php echo $thumbnail_url ?>'); background-repeat: no-repeat;
		background-position: 50% 50%;
		background-size: cover; min-height:700px;">
					</div>
				</div>
			</div>
			<div class="pb-contact-form wpb_column jt_col vc_column_container vc_col-sm-6 col-sm-6">
				<div class="vc_column-inner ">
					<div class="wpb_wrapper">
						<div class="wpb_text_column wpb_content_element pb-contact-info">
						<div class="wpb_wrapper">
							<h1><?php the_title(); ?></h1>
							<h2><?php echo get_post_meta(get_the_ID(), 'team_member_position', true); ?></h2>
							<p><?php the_content(); ?></p>
						</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
    <?php endwhile; ?>
    <?php endif; ?>
<?php get_footer(); ?>
