<?php get_header(); ?>
<?php $rebirth_jellythemes = rebirth_jellythemes_theme_options();?>
<!-- INTRO -->

<div class="intro jIntro">
    <div class="image-cover menu-bottom" style="background-image:url(<?php echo esc_url($rebirth_jellythemes['blog_header']['url']); ?>);">
        <div class="vcenter text-center">
            <div class="container">
                <div class="row visible">
                    <div class="col-md-8 col-md-offset-2">
                    <h1 class="primary-title invert"><?php echo wp_kses($rebirth_jellythemes['blog_title'], array('strong'=>array())); ?></h1>
                        <div class="voffset50"></div>
                        <p class="post-primary-title invert"><?php echo esc_html($rebirth_jellythemes['blog_subtitle']); ?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<section class="section">
    <div class="container container-full">
        <div class="row">
            <div class="col-md-12 jt_col column_container">
               <p class="title fz20 voffset150"><?php esc_html_e('The page cannot found', 'rebirth-jellythemes'); ?></p>
            </div>
        </div>
    </div>
</section>
<?php get_footer(); ?>
