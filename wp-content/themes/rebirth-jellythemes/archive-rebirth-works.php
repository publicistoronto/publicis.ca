<?php
    $counter = 1;
    $works = new WP_Query(array('post_type'=>'rebirth-works', 'posts_per_page' => $_GET['perpage'], 'paged' => $_GET['page']));
    if (isset($_GET['type']) && $_GET['type']!='undefined') {
        while ($works->have_posts()) : $works->the_post();
            $big = get_post_meta(get_the_ID(), '_rebirth_jellythemes_project_big', true );
            $return .= '<div class="thumbnail ' . implode(' ', get_post_class()) . ($big ? '' : ' small') . '">
                          ' .  wp_get_attachment_image( get_post_thumbnail_id(get_the_ID()), 'rebirth_jellythemes_project_thumb', true, array('class' => 'new')) . '
                          <div class="info-banner">
                            <div>
                                <div class="titleinfo">' . get_the_title() . '</div>
                                <div class="tagsinfo">' . get_post_meta($post->ID, '_rebirth_jellythemes_project_service', true ) . '</div>
                                <a href="' . esc_url(get_permalink()) .'" class="viewdetails">' . esc_html__('view details', 'rebirth-jellythemes') . '</a>
                            </div>
                        </div>';
        endwhile;
    } else {
        while ($works->have_posts()) : $works->the_post();
            $big = get_post_meta(get_the_ID(), '_rebirth_jellythemes_project_big', true );
            $return .= '<div class="thumbnail ' . implode(' ', get_post_class()) . ($big ? '' : ' small') . '">
                          ' .  wp_get_attachment_image( get_post_thumbnail_id(get_the_ID()), 'rebirth_jellythemes_project_thumb', true, array('class' => 'new')) . '
                          <div class="rollover rollover5">
                            <div class="vcenter">
                              <div class="title-project">' . get_the_title() . '</div>
                              <div class="tags-project">
                                ' . get_post_meta($post->ID, '_rebirth_jellythemes_project_service', true ) . '
                              </div>
                              <a href="' . esc_url(get_permalink()) .'" class="btn btn-default">' . esc_html__('view details', 'rebirth-jellythemes') . '</a>
                            </div>
                          </div>
                        </div>';
        endwhile;
    }
    echo $return;
?>
