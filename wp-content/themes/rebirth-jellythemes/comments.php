<?php
if ( post_password_required() ) {
	return;
}
?>
<ul class="commentslist"><?php wp_list_comments(array("callback"=> "rebirth_jellythemes_comments_format")); ?></ul>


<div class="voffset20"></div>

<nav class="pagination">
	<?php paginate_comments_links(); ?>
</nav>

<div class="voffset80"></div>


<?php comment_form(); ?>
