<!-- FOOTER -->
<?php $jellythemes = rebirth_jellythemes_theme_options(); ?>
<footer>
  <div class="container-fluid">
    <div class="voffset30"></div>
      <div class="row">
          <div class="col-md-12 col-sm-12 logo-container">
            <div class="voffset30"></div>
            <img src="<?php echo esc_url($jellythemes['logo_foot']['url']); ?>" alt="<?php echo strip_tags($jellythemes['blogname']); ?>">
            <p><?php echo esc_html(wp_kses(wpautop($jellythemes['footer_about']), array('br' => array(), 'strong' => array()))); ?></p>
          </div>
      </div>
      <div class="row">
      <div class="col-md-4 col-sm-12">
        <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Footer Content Col1") ) : ?>
		<?php endif;?>
      </div>
      <div class="col-md-4 col-sm-12">
       <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Footer Content Col2") ) : ?>
		<?php endif;?> 
      </div>
      <div class="col-md-4 col-sm-12">
       <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Footer Content Col3") ) : ?>
		<?php endif;?> 
      </div>
    </div>
    <div class="voffset30"></div>
  </div>
  <div class="copyright">
    <div class="voffset40"></div>
    <p><?php echo esc_html($jellythemes['copyright']); ?></p>
    <div class="voffset40"></div>
  </div>
</footer>
<?php if ($jellythemes['hamb_menu']): ?>
  <div class="overlay overlay-hugeinc">
  <?php wp_nav_menu(array(
          'container' => 'nav',
          'menu_class' => '',
          'container_id' => 'nav',
          'container_class' => '',
          'theme_location' => 'main',
          'walker' => new rebirth_jellythemes_walker_nav_menu)); ?>
  </div>
<?php endif ?>
<!-- SCRIPTS -->
<?php wp_footer(); ?>

</body>
</html>
