<?php

    /* INCLUDE OPTIONS FRAMEWORK */
    require_once( get_template_directory() . '/inc/admin/admin-init.php' );
    /* INCLUDE TGM PLUGIN ACTIVATION */
    require_once get_template_directory() . '/inc/class-tgm-plugin-activation.php';
	/* FRONTEND FUNCTIONS */
    include get_template_directory() . '/inc/functions.frontend.php';
    /* ADMIN PANEL FUNCTIONS */
    include get_template_directory() . '/inc/functions.admin.php';
	/* IMPORTING THE CUSTOM FILES */
	
	?>