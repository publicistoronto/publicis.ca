<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <?php $jellythemes = rebirth_jellythemes_theme_options(); ?>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php wp_head(); ?>
    <meta name="google-site-verification" content="nTfjXpePxMTdfRfHuCFsTXe963M2S2MZzvHiurTu96c" />
</head>
<body <?php body_class(); ?> >
  <!-- LOADER -->
  <div id="mask">
      <div class="loader"><img src="http://stg.publicis.ca/wp-content/themes/Publicis/images/1696064.gif" /></div>
  </div>
  <?php if ($jellythemes['hamb_menu']): ?>
  <button id="trigger-overlay" class="dropdown-icon dropdown-icon--animate invert">
    <span></span>
  </button>
  <?php endif; ?>

  <!-- HEADER -->
  <?php if ($jellythemes['hamb_menu']): ?>
    <header>
  <?php else : ?>
    <header id="jHeader" class="invert">
  <?php endif; ?>
    <nav class="navbar navbar-default">
      <div class="navbar-header">
        <?php if (!$jellythemes['hamb_menu']): ?>
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
            <span class="sr-only"><?php esc_html_e('Toggle', 'rebirth-jellythemes'); ?></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        <?php endif; ?>
        <a class="navbar-brand" href="<?php echo esc_url(home_url('/')); ?>"><img src="<?php echo esc_url($jellythemes['logo']['url']); ?>" alt="<?php echo strip_tags($jellythemes['blogname']); ?>"></a>
      </div>
      <?php if (!$jellythemes['hamb_menu']): ?>
        <?php $pid = !empty($post) ? get_post_meta($post->ID, '_rebirth_jellythemes_menu_name', true) : 0; ?>
        <?php wp_nav_menu(array(
                'container' => 'div',
                'menu_class' => 'nav navbar-nav navbar-right invert',
                'container_id' => 'navbar-rebirth',
                'container_class' => 'collapse navbar-collapse navbar-ex1-collapse',
                'theme_location' => 'main',
                'menu' => $pid,
                'walker' => new rebirth_jellythemes_walker_nav_menu)); ?>
      <?php endif ?>

    </nav>
  </header>
