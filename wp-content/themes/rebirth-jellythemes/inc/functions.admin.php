<?php
	// Setup theme
	add_action('after_setup_theme', 'rebirth_jellythemes_setup');
	function rebirth_jellythemes_setup(){
		// Load text domain
	    load_theme_textdomain('rebirth-jellythemes', get_template_directory() . '/languages');

	    // Theme supports
	    add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );
	    add_theme_support( 'automatic-feed-links' );
	    add_theme_support( "post-thumbnails" );
	    add_theme_support( 'title-tag' );
	}

	// menu's definition
	if ( function_exists( 'register_nav_menus' ) ) {
		register_nav_menus(
			array(
			  'main' =>  esc_html__('Main menu', 'rebirth-jellythemes'),
			  'footer' =>  esc_html__('Footer menu', 'rebirth-jellythemes')
			)
		);
	}
	// Add custom image sizes
	if ( function_exists( 'add_image_size' ) ) {
	    add_image_size('rebirth_jellythemes_project_thumb', 900, 864, true );
	    add_image_size('rebirth_jellythemes_project_list_thumb', 1800, 1800, false );
	    add_image_size('rebirth_jellythemes_blog_thumb', 1000, 600, true );
	    add_image_size('rebirth_jellythemes_blog_full', 1920, 1020, false );
	}

	// Filter to show custom tax clasess in post_class() function output
	add_filter( 'post_class', 'rebirth_jellythemes_custom_taxonomy_post_class', 10, 3 );
	if( !function_exists( 'rebirth_jellythemes_custom_taxonomy_post_class' ) ) {
	    function rebirth_jellythemes_custom_taxonomy_post_class( $classes, $class, $ID ) {
	        $terms = get_the_terms( (int) $ID, 'rebirth-type' );
	        if(is_object($terms) && !empty($terms)) {
	            foreach( (array) $terms as $order => $term ) {
	                if(isset($term->slug) && !in_array( $term->slug, $classes ) ) {
	                    $classes[] = $term->slug;
	                }
	            }
	        }
	        if (!has_post_thumbnail()) {
	        	$classes[] = 'no-thumbnail';
	        }
	    	return $classes;
	    }
	}

	//Registering plugin to require activation
	add_action( 'tgmpa_register', 'rebirth_jellythemes_register_plugins' );
	function rebirth_jellythemes_register_plugins() {
	    $plugins = array(
	        array(
	            'name'			=> esc_html__('WPBakery Visual Composer', 'rebirth-jellythemes'), // The plugin name
	            'slug'			=> 'js_composer',
	            'source'			=> get_stylesheet_directory() . '/plugins/js_composer.zip',
	            'required'			=> true,
	            'version'			=> '5.0.1',
	            'force_activation'		=> false,
	            'force_deactivation'	=> false,
	            'external_url'		=> '',
	        ),
	        array(
				'name' => esc_html__('Slider Revolution', 'rebirth-jellythemes'),
				'slug' => 'revslider',
				'source' => get_stylesheet_directory() . '/plugins/revslider.zip',
				'required' => false,
				'version' => '5.3.1.5',
				'force_activation' => false,
				'force_deactivation' => true,
				'external_url' => '',
			),
	        array(
	            'name'			=>  esc_html__('rebirth Theme Functionality', 'rebirth-jellythemes') ,
	            'slug'			=> 'rebirth-jellythemes-plugin',
	            'source'			=> get_stylesheet_directory() . '/plugins/rebirth-jellythemes-plugin.zip',
	            'required'			=> true,
	            'version'			=> '1.0.1',
	            'force_activation'		=> false,
	            'force_deactivation'	=> false,
	            'external_url'		=> '',
	        ),
	        array(
				'name' 	   => esc_html__('Redux Framework', 'rebirth-jellythemes'),
				'slug' 	   => 'redux-framework',
				'required' => true,
			),
	    );

	    $config = array(
	        'domain'		=> 'rebirth-jellythemes',
	        'default_path'		=> '',
	        'menu'			=> 'install-required-plugins',
	        'has_notices'		=> true,
	        'is_automatic'		=> true,
	        'message'		=> '',
	        'strings'		=> array(
	            'page_title'			=> esc_html__( 'Install Required Plugins', 'rebirth-jellythemes' ),
	            'menu_title'			=> esc_html__( 'Install Plugins', 'rebirth-jellythemes' ),
	            'installing'			=> esc_html__( 'Installing Plugin: %s', 'rebirth-jellythemes' ),
	            'oops'				=> esc_html__( 'Something went wrong with the plugin API.', 'rebirth-jellythemes' ),
	            'notice_can_install_required'	=> _n_noop( 'This theme requires the following plugin: %1$s.', 'This theme requires the following plugins: %1$s.', 'rebirth-jellythemes' ),
	            'notice_can_install_recommended'	=> _n_noop( 'This theme recommends the following plugin: %1$s.', 'This theme recommends the following plugins: %1$s.', 'rebirth-jellythemes' ),
	            'notice_cannot_install'		=> _n_noop( 'Sorry, but you do not have the correct permissions to install the %s plugin. Contact the administrator of this site for help on getting the plugin installed.', 'Sorry, but you do not have the correct permissions to install the %s plugins. Contact the administrator of this site for help on getting the plugins installed.', 'rebirth-jellythemes' ),
	            'notice_can_activate_required'	=> _n_noop( 'The following required plugin is currently inactive: %1$s.', 'The following required plugins are currently inactive: %1$s.', 'rebirth-jellythemes' ),
	            'notice_can_activate_recommended'	=> _n_noop( 'The following recommended plugin is currently inactive: %1$s.', 'The following recommended plugins are currently inactive: %1$s.', 'rebirth-jellythemes' ),
	            'notice_cannot_activate'		=> _n_noop( 'Sorry, but you do not have the correct permissions to activate the %s plugin. Contact the administrator of this site for help on getting the plugin activated.', 'Sorry, but you do not have the correct permissions to activate the %s plugins. Contact the administrator of this site for help on getting the plugins activated.', 'rebirth-jellythemes' ),
	            'notice_ask_to_update'		=> _n_noop( 'The following plugin needs to be updated to its latest version to ensure maximum compatibility with this theme: %1$s.', 'The following plugins need to be updated to their latest version to ensure maximum compatibility with this theme: %1$s.', 'rebirth-jellythemes' ), // %1$s = plugin name(s)
	            'notice_cannot_update'		=> _n_noop( 'Sorry, but you do not have the correct permissions to update the %s plugin. Contact the administrator of this site for help on getting the plugin updated.', 'Sorry, but you do not have the correct permissions to update the %s plugins. Contact the administrator of this site for help on getting the plugins updated.', 'rebirth-jellythemes' ), // %1$s = plugin name(s)
	            'install_link'			=> _n_noop( 'Begin installing plugin', 'Begin installing plugins', 'rebirth-jellythemes' ),
	            'activate_link'			=> _n_noop( 'Activate installed plugin', 'Activate installed plugins', 'rebirth-jellythemes' ),
	            'return'				=> esc_html__( 'Return to Required Plugins Installer', 'rebirth-jellythemes' ),
	            'plugin_activated'			=> esc_html__( 'Plugin activated successfully.', 'rebirth-jellythemes' ),
	            'complete'				=> esc_html__( 'All plugins installed and activated successfully. %s', 'rebirth-jellythemes' ), // %1$s = dashboard link
	            'nag_type'				=> 'updated' // Determines admin notice type - can only be 'updated' or 'error'
	        )
	    );
	    tgmpa( $plugins, $config );
	}

	/**
	 * Force Visual Composer to initialize as "built into the theme". This will hide certain tabs under the Settings->Visual Composer page
	 */
	if(function_exists('vc_set_as_theme')) vc_set_as_theme();


	/**
	* Return jellythemes options variable
	*/
	function rebirth_jellythemes_theme_options() {
		global $jellythemes;
		if (empty($jellythemes)) {
			$jellythemes['logo'] = array('url'=> get_stylesheet_directory_uri() . '/images/demo/logo-alt.png');
			$jellythemes['logo_foot'] = array('url'=> get_stylesheet_directory_uri() . '/images/demo/logo-alt.png');
			$jellythemes['blogname'] = 'rebirth';
			$jellythemes['color'] = '';
			$jellythemes['style'] = '';
			$jellythemes['blog_title'] = '';
			$jellythemes['copyright'] = '';
			$jellythemes['hamb_menu'] = false;
			$jellythemes['footer_form_title'] = '';
			$jellythemes['footer_form_text'] = '';
			$jellythemes['footer_address'] = '';
			$jellythemes['footer_phone'] = '';
			$jellythemes['footer_email'] = '';
			$jellythemes['dark'] = false;
			$jellythemes['footer_about'] = '';
			$jellythemes['blog_subtitle'] = '';
			$jellythemes['blog_header'] = array('url'=> '');
			$jellythemes['contact_email'] = get_option('admin_email');

			$jellythemes['footer_logo'] = array('url'=> get_stylesheet_directory_uri() . '/images/logo-footer.png');
		}
		return $jellythemes;
	}

	/**
	* REMOVE FRAMEBORDER FOR HTML VALIDATOR
	*/
	function rebirth_jellythemes_oembed( $return, $data, $url ) {
	 	$return = str_replace('frameborder="0" allowfullscreen', 'style="border: none"', $return);
		return $return;
	}
	add_filter('oembed_dataparse', 'rebirth_jellythemes_oembed', 90, 3 );

	function rebirth_jellythemes_remove_redux_notices() {
    if ( class_exists('ReduxFrameworkPlugin') ) {
        remove_filter( 'plugin_row_meta', array( ReduxFrameworkPlugin::get_instance(), 'plugin_metalinks'), null, 2 );
    }
    if ( class_exists('ReduxFrameworkPlugin') ) {
        remove_action('admin_notices', array( ReduxFrameworkPlugin::get_instance(), 'admin_notices' ) );
    }
}
add_action('init', 'rebirth_jellythemes_remove_redux_notices');

?>
