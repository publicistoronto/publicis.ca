<?php
    // Define content width
    if ( ! isset( $content_width ) ) $content_width = 1180;

    //Google fonts
    function rebirth_jellythemes_fonts_url() {
        $font_url = '';
        $jellythemes = rebirth_jellythemes_theme_options();

        if ( 'off' !== _x( 'on', 'Google font: on or off', 'rebirth-jellythemes' ) ) {
            $font_url = add_query_arg('family', urlencode('Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic|Roboto+Condensed:400,300,300italic,700,400italic,700italic|Nixie+One|Bitter:400,400italic,700|Dosis:200,300,400,500,600,700,800|Raleway:200,300,400,500,600,700'), "//fonts.googleapis.com/css" );
        }
        return $font_url;
    }

	// Load scripts and styles files
    function rebirth_jellythemes_scripts_and_styles() {
        $jellythemes = rebirth_jellythemes_theme_options();
        if (!is_admin()) {
            wp_enqueue_style( 'rebirth-jellythemes-fonts', rebirth_jellythemes_fonts_url(), array(), '1.0.0' );
            wp_enqueue_script('modernizr', get_template_directory_uri() . '/scripts/vendor/modernizr.js#asyncload', array(), '3.7.2');
            wp_enqueue_style('main',
                get_template_directory_uri() . '/styles/main.css');

            wp_enqueue_style( 'rebirth-jellythemes-style', get_stylesheet_uri() );
            
            wp_enqueue_script(
                'flickity',
                get_template_directory_uri() . '/scripts/plugins/flickity.pkgd.min.js',
                array('jquery'),false,true );
            wp_enqueue_script(
                'twitterFetcher',
                get_template_directory_uri() . '/scripts/plugins/twitterFetcher_min.js#asyncload',
                array('jquery'),false,true );
            wp_enqueue_script(
                'jquery-parallax',
                get_template_directory_uri() . '/scripts/plugins/jquery.parallax.min.js#asyncload',
                array('jquery'),false,true );
            wp_enqueue_script(
                'isotope',
                get_template_directory_uri() . '/scripts/plugins/isotope.pkgd.min.js',
                array('jquery'),false,true );
            wp_enqueue_script(
                'superslides',
                get_template_directory_uri() . '/scripts/plugins/jquery.superslides.min.js#asyncload',
                array('jquery'),false,true );
            wp_enqueue_script(
                'owl-carousel',
                get_template_directory_uri() . '/scripts/plugins/owl.carousel.min.js',
                array('jquery'),false,true );
            wp_enqueue_script(
                'inview',
                get_template_directory_uri() . '/scripts/plugins/jquery.inview.min.js',
                array('jquery'),false,true );
            wp_enqueue_script(
                'numscroller',
                get_template_directory_uri() . '/scripts/plugins/jquery.numscroller-1.0.js#asyncload',
                array('jquery'),false,true );
            wp_enqueue_script(
                'jquery.countdown-plugin',
                get_template_directory_uri() . '/scripts/jquery.countdown/jquery.plugin.min.js',
                array('jquery'),false,true );
            wp_enqueue_script(
                'jquery.countdown',
                get_template_directory_uri() . '/scripts/jquery.countdown/jquery.countdown.min.js',
                array('jquery'),false,true );
            wp_enqueue_script(
                'mb_YTPlayer',
                get_template_directory_uri() . '/scripts/YTPlayer.js#asyncload',
                array('jquery'),false,true );
            wp_enqueue_script(
                'bootstrap',
                get_template_directory_uri() . '/scripts/vendor/bootstrap.js',
                array('jquery'),false,true );
            $api_key = isset($jellythemes['api_key']) ? $jellythemes['api_key'] : '';
            wp_enqueue_script(
                'google-maps-api',
                '//maps.googleapis.com/maps/api/js?key=' . $api_key,
                array('jquery',),false,true );
            wp_enqueue_script(
                'rebirth-jellythemes-main',
                get_template_directory_uri() . '/scripts/main.js',
                array('jquery'),false,true );
            
        }
        if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
            wp_enqueue_script( 'comment-reply' );
        }
    }
    add_action('wp_enqueue_scripts', 'rebirth_jellythemes_scripts_and_styles');

    // Define walker nav menu to display custom html output
    class rebirth_jellythemes_walker_nav_menu extends Walker_Nav_Menu {

        function start_el( &$output, $item, $depth = 0, $args = array(), $curr = 0 ) {
            global $wp_query;
            $indent = ( $depth > 0 ? str_repeat( "\t", $depth ) : '' ); // code indent

            $depth_classes = array(
                ( $depth == 0 ? 'main-menu-item' : 'sub-menu-item' ),
                ( $depth >=2 ? 'sub-sub-menu-item' : '' ),
                ( $depth % 2 ? 'menu-item-odd' : 'menu-item-even' ),
                'menu-item-depth-' . $depth
            );
            $depth_class_names = esc_attr( implode( ' ', $depth_classes ) );

            $classes = empty( $item->classes ) ? array() : (array) $item->classes;
            $class_names = esc_attr( implode( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) ) );
            $class_names = str_replace('current_page_item', 'active', $class_names);
            if (strpos($item->url, '#')) {
                $class_names = str_replace('current-menu-item', '', $class_names);
                $class_names = str_replace('current', '', $class_names);
            }
            $output .= $indent . '<li id="nav-menu-item-'. $item->ID . '" class="page-scroll ' . $depth_class_names . ' ' . $class_names . '">';

            $attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
            $attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
            $attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
            $attributes .= ' class="menu-link ' . ( $depth > 0 ? 'sub-menu-link' : 'main-menu-link' ) . '"';

            $attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';

            global $wp;
            $attributes .= ! empty( $item->url )        ? ' data-target="'   . esc_attr( str_replace(home_url($wp->request . '/'), '', $item->url)) .'"' : '';

            $item_output = '';
            if (is_object($args)) :
            $item_output = sprintf( '%1$s<a%2$s>%3$s%4$s%5$s</a>%6$s',
                $args->before,
                $attributes,
                $args->link_before,
                apply_filters( 'the_title', $item->title, $item->ID ),
                $args->link_after,
                $args->after
            );
            endif;
            $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args, 0 );
        }
    }

    //Return array of section's IDs in Main menu
    function rebirth_jellythemes_get_sections(){
        if(!has_nav_menu( 'main' )) {
            return;
        }
        if ( ( $locations = get_nav_menu_locations() ) && $locations['main']  ) {
            $menu = wp_get_nav_menu_object( $locations['main'] );
            $items  = wp_get_nav_menu_items($menu->term_id);
            $sections = array();
            foreach((array) $items as $menu_items){
                if ($menu_items->object == 'page-sections') {
                    $sections[] = $menu_items->object_id;
                }
            }
        }
        return $sections;
    }

    //Comment format and Walker
    function rebirth_jellythemes_comments_format($comment, $args, $depth) {
            $id = $comment->comment_ID;
    ?>
        <li <?php comment_class(empty( $args['has_children'] ) ? '' : 'parent') ?>>
            <div <?php comment_class(empty( $args['has_children'] ) ? 'media' : 'media parent') ?> id="comment-<?php echo esc_attr($id); ?>">
                <a class="pull-left" href="#">
                <?php if ($args['avatar_size'] != 0) echo get_avatar( $comment, 110, '', false, array('class' => 'banner blog-post-author')); ?>
                </a>
                <div class="media-body">
                    <h4 class="media-heading">
                        <?php printf( esc_html__('%1$s at %2$s', 'rebirth-jellythemes'), get_comment_date(),  get_comment_time()); ?>
                        <br>
                        <?php comment_author_link(); ?>
                    </h4>
                    <?php if ($comment->comment_approved == '0') : ?>
                    <span class="comment-awaiting-moderation"><?php echo esc_html__('Your comment is awaiting moderation.', 'rebirth-jellythemes') ?></span>
                    <br/>
                    <?php endif; ?>
                    <?php comment_text(); ?>
                    <?php comment_reply_link(array_merge( $args, array('add_below' => "div-comment", 'depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
                </div>
                <hr>
            </div>
    <?php
    }

    add_filter('comment_reply_link', 'rebirth_jellythemes_replace_reply_link_class');
    function rebirth_jellythemes_replace_reply_link_class($class){
        $class = str_replace("class='comment-reply-link", "class='reply", $class);
        return $class;
    }

    //Format title
    function rebirth_jellythemes_wp_title( $title, $sep ) {
        $jellythemes = rebirth_jellythemes_theme_options();

        // Add the site name.
        $title .= strip_tags($jellythemes['blogname']);

        return $title;
    }
    add_filter( 'wp_title', 'rebirth_jellythemes_wp_title', 10, 2 );


    //Comments form structure
    add_filter( 'comment_form_fields', 'rebirth_jellythemes_move_comment_field_to_bottom' );
    function rebirth_jellythemes_move_comment_field_to_bottom( $fields ) {
        $comment_field = $fields['comment'];
        unset( $fields['comment'] );
        $fields['comment'] = $comment_field;
        return $fields;
    }


    add_filter( 'comment_form_default_fields', 'rebirth_jellythemes_comment_form_fields' );
    function rebirth_jellythemes_comment_form_fields( $fields ) {
        $commenter = wp_get_current_commenter();

        $req      = get_option( 'require_name_email' );
        $aria_req = ( $req ? " aria-required='true'" : '' );
        $html5    = current_theme_supports( 'html5', 'comment-form' ) ? 1 : 0;
        $fields   =  array(
            'author' => '<div class="form-group inline"><input placeholder="' . esc_html__( 'Name', 'rebirth-jellythemes' ) . ( $req ? ' *' : '' ) . '" class="form-control" id="author" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) . '" size="30"' . $aria_req . ' /></div>',
            'email'  => '<div class="form-group inline"><input placeholder="' . esc_html__( 'Email', 'rebirth-jellythemes' ) . ( $req ? ' *' : '' ) . '" id="email" name="email" ' . ( $html5 ? 'type="email"' : 'type="text"' ) . ' value="' . esc_attr(  $commenter['comment_author_email'] ) . '" class="form-control"' . $aria_req . ' /></div>',
            'url'    => '<div class="form-group"><input class="form-control" id="url" placeholder="' . esc_html__( 'Website', 'rebirth-jellythemes' ) . ( $req ? ' *' : '' ) . '" name="url" ' . ( $html5 ? 'type="url"' : 'type="text"' ) . ' value="' . esc_attr( $commenter['comment_author_url'] ) . '" size="30" /></div>'
        );


        return $fields;
    }
    add_filter( 'comment_form_defaults', 'rebirth_jellythemes_comment_form' );
    function rebirth_jellythemes_comment_form( $args ) {
        $args['comment_field'] = '<div class="form-group"><textarea name="comment" id="comment" class="form-control" rows="3" placeholder="' . esc_html__( 'Comment', 'rebirth-jellythemes' ) . '"></textarea></div>';
        $args['class_submit'] = 'btn btn-arrow'; // since WP 4.1
        $args['submit_button']= '<div class="voffset20"></div><button name="%1$s" type="submit" id="%2$s" class="%3$s">%4$s</button>';
        $args['title_reply_before'] = '<h3 id="reply-title" class="title-form">';
        $args['comment_notes_before'] = '';

        $args['submit_field'] = '%1$s %2$s';

        return $args;
    }


    add_filter( 'body_class','rebirth_jellythemes_body_classes' );
    function rebirth_jellythemes_body_classes( $classes ) {
        global $post;
        $jellythemes = rebirth_jellythemes_theme_options();
        $classes[] = $jellythemes['dark'] ? 'dark' : '';
        $classes[] = has_post_thumbnail($post->ID) ? '' : 'no-thumbnail';
        return $classes;

    }

    // Widgets zones definition
    function rebirth_jellythemes_widgets_init() {
        register_sidebar(array(
            'id' => 'rebirth-jellythemes-sidebar',
            'before_widget' => '<div id="%1$s" class="widget %2$s">',
            'after_widget' => '</div>',
            'before_title' => '<div class="title small">',
            'after_title' => '</div>',
            'name' => esc_html__('Blog sidebar', 'rebirth-jellythemes')
        ));

    }
    add_action('widgets_init', 'rebirth_jellythemes_widgets_init');


    function rebirth_jellythemes_pagination()
    {
        if (is_singular()) {
            return;
        }
        global $wp_query;
        /** Stop execution if there's only 1 page */
        if ($wp_query->max_num_pages <= 1) {
            return;
        }
        $paged = get_query_var('paged') ? absint(get_query_var('paged')) : 1;
        $max = intval($wp_query->max_num_pages);
        /** Add current page to the array */
        if ($paged >= 1) {
            $links[] = $paged;
        }
        /** Add the pages around the current page to the array */
        if ($paged >= 3) {
            $links[] = $paged - 1;
            $links[] = $paged - 2;
        }
        if (($paged + 2) <= $max) {
            $links[] = $paged + 2;
            $links[] = $paged + 1;
        }
        echo '<div class="pagination_wrapper"><ul class="pagination">' . "\n";
        /** Previous Post Link */
        if (get_previous_posts_link()) {
            printf('<li>%s</li>' . "\n", get_previous_posts_link("<<"));
        }
        /** Link to first page, plus ellipses if necessary */
        if (!in_array(1, $links)) {
            $class = 1 == $paged ? ' class="first active"' : ' class="first"';
            printf('<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url(get_pagenum_link(1)), '1');
            if (!in_array(2, $links)) {
                echo '<li><span class="btn disabled">...</span></li>' . "\n"; // Here is the correction
            }
        }
        /** Link to current page, plus 2 pages in either direction if necessary */
        sort($links);
        foreach ((array)$links as $link) {
            $class = $paged == $link ? ' class="last active"' : ' class="last"';
            printf('<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url(get_pagenum_link($link)), $link);
        }
        /** Link to last page, plus ellipses if necessary */
        if (!in_array($max, $links)) {
            if (!in_array($max - 1, $links)) {
                echo '<li><span class="btn disabled">...</span></li>' . "\n";
            }
            $class = $paged == $max ? ' class="active"' : '';
            printf('<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url(get_pagenum_link($max)), $max);
        }
        /** Next Post Link */
        if (get_next_posts_link()) {
            printf('<li>%s</li>' . "\n", get_next_posts_link(">>"));
        }
        echo '</ul></div>' . "\n";
    }

    add_filter('next_posts_link_attributes', function() {
        return 'class="paginationblog newpost"';
    });
    add_filter('previous_posts_link_attributes', function() {
        return 'class="paginationblog oldpost"';
    });

?>
