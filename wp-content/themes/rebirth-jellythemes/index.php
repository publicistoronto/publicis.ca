<?php get_header(); ?>
<?php $rebirth_jellythemes = rebirth_jellythemes_theme_options();?>
<!-- INTRO -->
<?php if (!empty($rebirth_jellythemes['blog_header']['url'])): ?>
    <div class="intro jIntro">
        <div class="image-cover menu-bottom" style="background-image:url(<?php echo esc_url($rebirth_jellythemes['blog_header']['url']); ?>);">
            <div class="vcenter text-center">
                <div class="container">
                    <div class="row visible">
                        <div class="col-md-8 col-md-offset-2">
                        <h1 class="primary-title invert"><?php echo wp_kses($rebirth_jellythemes['blog_title'], array('strong'=>array())); ?></h1>
                            <div class="voffset50"></div>
                            <p class="post-primary-title invert"><?php echo esc_html($rebirth_jellythemes['blog_subtitle']); ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif ?>
<section class="section">
    <div class="container container-full">
    	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
            <div class="row row-no-gutter visible">
                <div class="col-md-6">
                    <div class="banner blog-2-image" style="background-image:url(<?php the_post_thumbnail_url('rebirth_jellythemes_blog_thumb') ?>);"></div>
                </div>
                <div class="col-md-6">
                    <div class="blog-2-text">
                        <div class="vcenter blog-post-content">
                            <div class="blog-post-header">
                                <div class="blog-post-author"></div>
                                <p class="blog-post-date"><?php the_time(get_option('date_format')) ?>  / <?php the_author_link() ?></p>
                            </div>
                            <div class="voffset40"></div>
                            <h1 class="blog-post-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
                            <div class="voffset20"></div>
                            <p class="blog-post-intro"><?php the_excerpt(); ?></p>
                            <div class="voffset40"></div>
                            <div class="post-share">
                                <?php the_category(); ?>
                            </div>
                            <div class="voffset50"></div>
                            <a href="<?php the_permalink(); ?>" class="readfull"><?php esc_html_e('read full article', 'rebirth-jellythemes'); ?></a>
                            <div class="voffset50"></div>
                        </div>
                    </div>
                </div>
            </div>
    	<?php endwhile; ?>
        <div class="row row-no-gutter visible">
            <div class="col-sm-6">
                <?php previous_posts_link(esc_html__('previous posts', 'rebirth-jellythemes')); ?>
            </div>
            <div class="col-sm-6">
                <?php next_posts_link(esc_html__('next posts', 'rebirth-jellythemes')); ?>
            </div>
        </div>
    	<?php else: ?>
    		<div class="row">
	    		<div class="col-md-12 jt_col column_container">
	               <p class="title fz20 voffset150"><?php esc_html_e('There are no results for your query', 'rebirth-jellythemes'); ?></p>
	            </div>
            </div>
    	<?php endif; ?>
    </div>
</section>
<?php get_footer(); ?>
