<?php /* Template Name: Page Contact*/ ?>
<?php get_header(); ?>
    <?php if (function_exists('rwmb_meta')): ?>
        <?php while ( have_posts() ) : the_post(); ?>
            <?php $images = rwmb_meta('_rebirth_jellythemes_slider_images_contact', 'type=image', $post->ID ); ?>
            <?php foreach ($images as $image) : ?>
                <?php $slide = $image['full_url']; ?>
            <?php endforeach; ?>
            <div class="intro jIntro">
                <div class="image-cover" style="background-image: url(<?php echo esc_url($slide); ?>)">
                    <div class="vcenter text-center">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-8 col-md-offset-2">
                                    <div class="voffset50"></div>
                                    <p class="pretitle invert superbig"><?php echo get_post_meta( $post->ID, '_rebirth_jellythemes_contact_pretitle', true ); ?></p>
                                    <div class="voffset80"></div>
                                    <h1 class="primary-title invert"><strong><?php echo get_post_meta( $post->ID, '_rebirth_jellythemes_contact_title', true ); ?></strong></h1>
                                    <div class="voffset80"></div>
                                    <p class="post-primary-title invert"><?php echo get_post_meta( $post->ID, '_rebirth_jellythemes_contact_subtitle', true ); ?></p>
                                    <div class="voffset10"></div>
                                    <p class="subtitle address invert"><?php echo get_post_meta( $post->ID, '_rebirth_jellythemes_contact_subtitle2', true ); ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php endwhile; ?>
        <?php $back = $post //backup post data?>
        <?php get_template_part('templates/loop', 'pages'); ?>
        <?php $post = $back //restore post data ?>
    <?php endif ?>
<?php get_footer(); ?>
