<?php /* Template Name: Page Photo Slider */ ?>
<?php get_header(); ?>
    <?php if (function_exists('rwmb_meta')): ?>
        <?php while ( have_posts() ) : the_post(); ?>
            <!-- INTRO -->
            <?php $revslider = get_post_meta( $post->ID, '_rebirth_jellythemes_revslider', true ); ?>
            <?php $tiny = get_post_meta( $post->ID, '_rebirth_jellythemes_slider_tiny', true ); ?>
            <?php if (!empty($revslider)): ?>
                <?php echo do_shortcode($revslider); ?>
            <?php else : ?>
                <section class="intro full-width jIntro <?php echo ($tiny ? 'tiny' : ''); ?>" id="<?php echo esc_attr($post->post_name); ?>">
                    <div class="container-full">
                        <div class="row row-no-gutter">
                            <div class="col-md-12">
                                <div class="slider-intro">
                                    <div id="slides">
                                        <div class="slides-container">
                                            <?php $images = rwmb_meta('_rebirth_jellythemes_slider_images', 'type=image', $post->ID ); ?>
                                            <?php foreach ($images as $image) : ?>
                                                <img src="<?php echo esc_url($image['full_url']); ?>" alt="slide-<?php echo esc_html($image['ID']); ?>">
                                            <?php endforeach; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="vcenter text-center text-overlay">
                            <?php $texts =  get_post_meta( $post->ID, '_rebirth_jellythemes_slider_text', true ); ?>
                            <div id="owl-main-text" class="owl-carousel">
                                <?php foreach ($texts as $i => $text) : ?>
                                    <div class="item">
                                        <h1 class="primary-title invert"><?php echo wp_kses($text, array('strong' => array())); ?></h1>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                            <div class="voffset20"></div>
                            <a href="<?php echo esc_url(get_post_meta( $post->ID, '_rebirth_jellythemes_slider_button_link_1', true )); ?>" class="btn btn-invert"><?php echo get_post_meta( $post->ID, '_rebirth_jellythemes_slider_button_1', true ); ?></a>
                        </div>
                    </div>
                </section>
            <?php endif ?>
        <?php endwhile; ?>
        <?php $back = $post //backup post data?>
        <?php get_template_part('templates/loop', 'pages'); ?>
        <?php $post = $back //restore post data ?>
    <?php endif ?>
<?php get_footer(); ?>
