<?php get_header(); ?>
	<div class="section blog single-post" id="anchor07">
		<div class="container">
			<div class="row">
				<div class="col-md-9">
					<article <?php post_class('post-details'); ?>>
							<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
							<?php the_post_thumbnail('rebirth_jellythemes_event_image', array('class' => 'featured-image')); ?>
							<h4 class="title small"><span class="gray"><?php esc_html_e('Posted by:', 'rebirth-jellythemes'); ?></span> <?php the_author_link() ?> | <?php the_category(', '); ?> <span class="right"><i class="fa fa-commenting-o"></i> <?php comments_popup_link(); ?></span></h4>
							<h1 class="title post-detail"><?php the_title(); ?></h1>

							<?php the_content(); ?>
		                    <?php wp_link_pages(); ?>

							<div class="voffset120"></div>
							<?php if (get_the_author_meta('description')): ?>
								<div class="post-author">
									<h4 class="title small"><?php esc_html_e('about the author', 'rebirth-jellythemes'); ?></h4>
									<div class="media">
										<a class="pull-left" href="#">
											<?php echo get_avatar(get_the_author_meta('ID'), 111, '', false, array('class' => 'media-object')); ?>
										</a>
										<div class="media-body">
											<h4 class="media-heading"><?php the_author_link() ?></h4>
											<?php echo get_the_author_meta('description')  ?>
										</div>
									</div>
								</div>
							<?php endif ?>


							<div class="voffset60"></div>
						<?php endwhile; ?>
						<?php endif; ?>
						<?php comments_template(); ?>
					</article>
				</div>

				<div class="col-md-3">
					<?php get_sidebar(); ?>
				</div>
			</div>
		</div>
	</div>

<?php get_footer(); ?>
