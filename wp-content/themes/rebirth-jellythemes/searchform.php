<form method="get" id="searchform" class="input-group search" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<label for="s" class="hide assistive-text"><?php esc_html_e( 'Search', 'rebirth-jellythemes' ); ?></label>
	<input type="text" class="form-control" name="s" id="s" placeholder="<?php esc_attr_e( 'Search', 'rebirth-jellythemes' ); ?>" />
	<span class="input-group-btn">
      <button type="submit">
        <i class="fa fa-search"></i>
      </button>
    </span>
</form>
