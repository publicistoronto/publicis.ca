<?php get_header(); ?>
	<!-- intro -->
	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
		<div class="intro jIntro nointro"> <!-- menu-bottom -->
		</div>

		<div class="section slider-project">
			<div class="container">
                <div class="voffset140"></div>
                <div class="row">
				    <div class="col-md-4 slider-info-container">
                        <h1 class="post-primary-title normal center"><?php the_title(); ?></h1>
                        <p class="works-description normal center"><?php the_field('works-description', $term); ?></p>
                    </div>
                    <div class="col-md-8 workslider">
						<?php the_field('workslider', $term); ?>
					</div>
                </div>
                <div class="voffset80"></div>
			</div>
		</div>

        <div class="client-bar">
            <h2 class="client-name normal center"><?php echo get_post_meta($post->ID, '_rebirth_jellythemes_project_client', true ) ?></h2>      
        </div>

		<div class="section">
			<div class="container-fluid container-full">
                <div class="col-md-12">
                    <ul class="infowork menu-style">
                        <li><span><?php esc_html_e('Client:', 'rebirth-jellythemes'); ?></span> <?php echo get_post_meta($post->ID, '_rebirth_jellythemes_project_client', true ) ?></li>
                        <li><span><?php esc_html_e('Services:', 'rebirth-jellythemes'); ?></span> <?php echo get_post_meta($post->ID, '_rebirth_jellythemes_project_service', true ); ?></li>
                        <li><span><?php esc_html_e('Website:', 'rebirth-jellythemes'); ?></span>  <a href="<?php echo esc_url(get_post_meta($post->ID, '_rebirth_jellythemes_project_url', true )) ?>"><?php esc_html_e('View online', 'rebirth-jellythemes'); ?></a></li>
                    </ul>
                </div>
			</div>
		</div>
        
        <div class="section works-tab">
            <div class="col-md-12 clearfix">
                <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#tab1" aria-controls="tab1" role="tab" data-toggle="tab"><?php the_field('tab1', $term); ?></a></li>
            <li role="presentation"><a href="#tab2" aria-controls="tab2" role="tab2" data-toggle="tab"><?php the_field('tab2', $term); ?></a></li>
            <li role="presentation"><a href="#tab3" aria-controls="tab3" role="tab" data-toggle="tab"><?php the_field('tab3', $term); ?></a></li>
          </ul>
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="tab1">
                        <div class="col-md-6 img-container" style="background:url(<?php the_field('image1', $term); ?>) no-repeat left center"></div>
                        <div class="col-md-6 desc-container">
                            <?php the_field('text1', $term); ?>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="tab2">
                        <div class="col-md-6 img-container" style="background:url(<?php the_field('image2', $term); ?>) no-repeat left center"></div>
                        <div class="col-md-6 desc-container">
                            <?php the_field('text2', $term); ?>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="tab3">
                        <div class="col-md-6 img-container" style="background:url(<?php the_field('image3', $term); ?>) no-repeat left center"></div>
                        <div class="col-md-6 desc-container">
                            <?php the_field('text3', $term); ?>
                        </div>
                    </div>
                  </div>
            </div>

        </div>
        
	<?php endwhile; ?>
	<?php endif; ?>
    

<?php get_footer(); ?>
