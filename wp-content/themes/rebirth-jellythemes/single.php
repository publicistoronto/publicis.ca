<?php get_header(); ?>
	<!-- intro -->
	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
		<?php if (has_post_thumbnail()): ?>
			<div class="intro jIntro">
		      <div class="image-cover menu-bottom" style="background-image:url(<?php the_post_thumbnail_url('rebirth_jellythemes_blog_full') ?>);">
		      </div>
		    </div>
		<?php endif ?>
	    <div <?php post_class('section post-whidout-sidebar'); ?>>
	    	<div class="container">
	    		<div class="row">
	    			<div class="col-md-9 col-md-offset-3">
	    				<div class="content-post post-without-sidebar">
	    					<div class="post-extra">
	    						<div class="blog-post-author"></div>
	    						<a href="#comments" class="button-blog ico-comments"></a>
	    					</div>

	    					<p class="blog-post-date block"><?php esc_html_e('by', 'rebirth-jellythemes'); ?> <?php the_author_link(); ?> <?php esc_html_e('on', 'rebirth-jellythemes'); ?> <?php the_date(get_option('date_format')) ?></p>
	    					<h1 class="primary-title text-left"><?php the_title(); ?></h1>
	    					<div class="post-details">
	    						<?php the_content(); ?>
	    					</div>

	    					<!-- intro comments -->
	    					<div class="intro-comments">
	    						<div class="count-comments">
	    							<span class="number"><?php comments_number('0', '1', '%') ?></span> <?php esc_html_e('comments', 'rebirth-jellythemes'); ?>
	    							<a href="#respond" class="leave-reply"><?php esc_html_e('leave a reply', 'rebirth-jellythemes'); ?></a>
	    						</div>
	    						<ul class="share">
	    							<?php the_tags(); ?>
	    						</ul>
	    					</div>
	    					<div id="comments"></div>
							<?php comments_template(); ?>
	    				</div>
	    			</div>
	    		</div>
	    	</div>
	    </div>
	<?php endwhile; ?>
	<?php endif; ?>
	<div class="section">
		<div class="container container-full">
			<div class="row row-no-gutter">
				<div class="col-sm-6">
					<?php $prev_post = get_previous_post(); ?>
					<?php  if ($prev_post) : ?>
						<div class="paginationblog includeimage oldpost">
							<a href="<?php echo get_permalink($prev_post->ID) ?>">
								<?php $thumb = wp_get_attachment_image_src(get_post_thumbnail_id($prev_post->ID ), 'rebirth_jellythemes_blog_thumb'); ?>
								<div class="banner" style="background-image:url(<?php echo esc_url( $thumb[0]) ?>)">
									<div class="voffset160"></div>
								</div>
								<p class="info">
									<span class="date-pagination"><?php echo get_the_date(get_option('date_format'), $prev_post) ?></span>
									<span class="title-pagination"><?php echo get_the_title($prev_post->ID) ?></span>
								</p>
							</a>
						</div>
					<?php endif; ?>
				</div>
				<div class="col-sm-6">
					<?php $next_post = get_next_post(); ?>
					<?php  if ($next_post) : ?>
						<div class="paginationblog includeimage newpost">
							<a href="<?php echo get_permalink($next_post->ID) ?>">
								<?php $thumb = wp_get_attachment_image_src(get_post_thumbnail_id($next_post->ID ), 'rebirth_jellythemes_blog_thumb'); ?>
								<div class="banner" style="background-image:url(<?php echo esc_url( $thumb[0]) ?>)">
									<div class="voffset160"></div>
								</div>
								<p class="info">
									<span class="date-pagination"><?php echo get_the_date(get_option('date_format'), $next_post) ?></span>
									<span class="title-pagination"><?php echo get_the_title($next_post->ID) ?></span>
								</p>
							</a>
						</div>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
<?php get_footer(); ?>
