<?php $child_sections = new WP_Query(array('post_type' => 'page', 'post_parent' => $post->ID, 'orderby' => 'menu_order', 'order' =>'ASC', 'posts_per_page' => -1)); ?>
<?php while ($child_sections->have_posts() ) : $child_sections->the_post(); ?>
    <?php $bg = rwmb_meta( '_rebirth_jellythemes_section_bg', 'type=image', get_the_ID() );   foreach ($bg as $bg_image) :
            $bg_url = $bg_image['full_url']; endforeach; ?>
            <?php $pattern = get_post_meta( $post->ID, '_rebirth_jellythemes_pattern', true ); ?>
        <section id="<?php echo esc_attr($post->post_name); ?>"
            class="section
                    <?php echo get_post_meta( $post->ID, '_rebirth_jellythemes_section_color', true ); ?>
                    <?php echo get_post_meta( $post->ID, '_rebirth_jellythemes_section_type', true ); ?>
                    <?php echo get_post_meta( $post->ID, '_rebirth_jellythemes_section_width', true ); ?>
                    <?php echo ($pattern ? 'pattern' : ''); ?>"
            <?php echo (!empty($bg_url) ? 'data-parallax-image="' . $bg_url . '"'  : ''); ?>
            style="background-color:<?php echo get_post_meta( $post->ID, '_rebirth_jellythemes_bg_color', true ); ?>;
            <?php echo (!empty($bg_url) ? 'background-image: url(' . $bg_url . ')' : ''); ?>">
                <div class="container">
                    <?php the_content(); ?>
                </div>
        </section>
    <?php $bg_url=''; ?>
    <?php $custom_css = get_post_meta( $post->ID, '_wpb_shortcodes_custom_css', true ); ?>
    <?php if (!empty($custom_css)): ?>
        <style type="text/css" data-type="vc_custom-css">
            <?php echo $custom_css; ?>
        </style>
    <?php endif ?>
<?php endwhile; ?>
