<?php $limit =  get_post_meta(get_the_ID(), '_rebirth_jellythemes_portfolio_limit', true ); ?>
<div class="section">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <ul class="filters">
                    <li data-filter="*" class="is-checked wow fadeInUp" data-wow-delay="0.5s">
                        <span><?php esc_html_e('filter portfolio', 'rebirth-jellythemes'); ?></span>
                    </li>
                    <?php $types = get_terms('rebirth-type', array('hide_empty'=>0)); ?>
                    <?php if ( $types && ! is_wp_error( $types ) ) : ?>
                        <?php foreach ( $types as $type ) : ?>
                            <li data-filter=".<?php echo esc_js($type->taxonomy) . '-' . esc_js($type->slug); ?>" class="wow fadeInUp" data-wow-delay="0.8s"><?php echo $type->name; ?></li>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </ul>
            </div>
        </div>
        <?php $works = new WP_Query(array('post_type'=>'rebirth-works', 'posts_per_page' => $limit)); ?>
        <div class="row">
            <div class="thumbnails work1">
            <?php while ($works->have_posts()) : $works->the_post(); ?>
                <?php $big = get_post_meta(get_the_ID(), '_rebirth_jellythemes_project_big', true ); ?>
                <div <?php post_class('thumbnail ' . ($big ? '' : 'small')) ?>>
                    <?php $image = rwmb_meta('_rebirth_jellythemes_project_featured', 'type=image', $post->ID ); ?>
                    <?php if (!(empty($image))): ?>
                        <?php foreach ($image as $featured): ?>
                            <?php echo wp_get_attachment_image($featured['ID'],'rebirth_jellythemes_project_list_thumb') ?>
                        <?php endforeach ?>
                    <?php else : ?>
                        <?php the_post_thumbnail('rebirth_jellythemes_project_list_thumb'); ?>
                    <?php endif ?>
                    <div class="info-banner">
                        <div>
                            <div class="titleinfo"><?php the_title(); ?></div>
                            <div class="tagsinfo"><?php echo get_post_meta(get_the_ID(), '_rebirth_jellythemes_project_service', true ) ?></div>
                            <a href="<?php the_permalink(); ?>" class="viewdetails"><?php esc_html_e('view details', 'rebirth-jellythemes'); ?></a>
                        </div>
                    </div>
                </div>
            <?php endwhile; ?>
            </div>
        </div>
        <div class="voffset50"></div>
        <?php if ($limit < $works->found_posts): ?>
            <p class="loadmore big">
                <a data-type="minimal" data-perpage="<?php echo esc_js($limit); ?>" data-total="<?php echo esc_js($works->found_posts); ?>" href="<?php echo get_post_type_archive_link('rebirth-works'); ?>" id="more-works" href="#" class="btn rounded border"><?php esc_html_e('load more', 'rebirth-jellythemes'); ?></a>
            </p>
            <div class="voffset80"></div>
        <?php endif ?>
    </div>
</div>
