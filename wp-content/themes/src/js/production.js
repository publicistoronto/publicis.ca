(function ($) {
	'use strict';
	// scroll magic init
	$(function () { // wait for document ready
		// init
		var controller = new ScrollMagic.Controller();

		// define movement of panels
		var wipeAnimation = new TimelineMax()
			.fromTo("section.panel.left", 1, {x: "-100%"}, {x: "0%", ease: Linear.easeNone}, 0)  // in from left
			.fromTo("section.panel.right", 1, {x:  "100%"}, {x: "0%", ease: Linear.easeNone}, 0);

		// create scene to pin and link animation
		new ScrollMagic.Scene({
			triggerElement: "#pinContainer",
			triggerHook: 'onLeave',
			duration: "350"
		})
		.setPin("#pinContainer")
		.setTween(wipeAnimation)
		.addTo(controller);
	});

	// watch user idle for recaptcha view state
	var idleState = false;
	var idleDuration = null;
	$('*').bind('click keydown keypress keyup submit resize dblclick', function () {
		if($('body').hasClass('page-template-page-template-production')){
			clearTimeout(idleDuration);
			if (idleState == true) {
				$('.grecaptcha-badge').fadeIn();
			}
			idleState = false;
			idleDuration = setTimeout(function () {
				if($('.grecaptcha-badge').attr('style')){
					$('.grecaptcha-badge').fadeOut();
				} else{
					$('.grecaptcha-badge').fadeIn();
				}
				idleState = true; 
			}, 3000);
		}
	
	});
	$("body").trigger("mousemove");

	// adjust document scrolling position when clicking menu
	$('#menu-production-menu .main-menu-link').on('click', function(e){ 
		e.preventDefault();

		var id = $(this).data('target').split('/').pop();
		var top = $(id).offset().top;
		var offset = document.getElementById('jHeader').getBoundingClientRect().height;

		$('body, html').animate({scrollTop: top - offset}, 0);
	});

	// adjust document scrolling position when clicking tab module CTA
	$('.tab-sections .section-cta .vc_btn3').on('click', function (e) {
		e.preventDefault();

		var id = $(this).attr("href").split('/').pop();
		var top = $(id).offset().top;
		var offset = document.getElementById('jHeader').getBoundingClientRect().height;

		$('body, html').animate({ scrollTop: top - offset }, 0);
	});
	
     
	// dropdown menu for tabs-section in Diigtal Module
	$(function () {
		$('.tab-sections .vc_tta-tabs-container').on('click', function () {
			$(this).toggleClass('dropdown-visible'); 
		});
	});



})(jQuery);